## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app packager in the development mode.
Make sure you have already installed the app's debugRelease before running `npm start`
you can also run the packager with `react-native start`
You can reload the packager from within the app by pressing R twice.

### `npm test`

Launches the test runner.

### `npm run clean`

cleans android project build.

### `npm run build`

builds android project.

### `npm run bundle`

bundles the source files for production mode, bundle file can be found
inside the assets in android project.
Note that the project should be built and fully packaged before running `npm run bundle`

## API Keys
### Google map
Managed under info@manandvanworld.com account in GSuite.
https://console.developers.google.com/apis/credentials?project=man-and-van-238423&supportedpurview=project

## Twilio
Sending SMS messages
https://www.twilio.com/login

## SendGrid
Sending Email messages

## Google Play Store


## Apple iTunes Store


## Visrtual server host
Hosted by AWS, root account is your main email address
