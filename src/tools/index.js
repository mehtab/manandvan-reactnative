import axios from "axios"
import call from 'react-native-phone-call'

export let request = (url, data, callback, method) => {
    data.token_id = env.apiToken;
    if (!method) method = "post"
    if (method == "post")
        axios.post(env.apiUri + url, data).then(result => callback(null, result.data)).catch(e => callback(e))
    else if (method == "get")
    {
        axios.get(env.apiUri + url, data).then(result => callback(null, result.data)).catch(e => callback(e))
    }
        
}

export let getOneTimeToken = ()=>{
    return new Promise((resolve,reject)=>{
        request("/app/api/v1/paypal/token",{}, (error, data)=>{
            if(error || data.status !== "success"){
                reject(error);
            }
            resolve(data.data.authentication_token);
        });
    });
}

export let sendBookingRequest = (data)=>{
    return new Promise((resolve, reject)=>{
        request("/app/api/v1/booking/add", data , (error, data)=>{
            if(error || data.status !== "success"){
                console.log("booking api error : ", error)
                console.log("data : ", data)
                alert("Something went wrong")
                reject(false);
            }
            console.log("response of booking success: ", data)
            resolve(true);
        });
    });
}


export let verifyVendorCode = (data)=>{
    return new Promise((resolve, reject)=>{

         data =  {params: {supplier_id: data.supplier_id}}
        request("/admin/api/v1/supplier/userid/", data , (error, data)=>{
            if(error || data.status !== "success"){
                alert("Invalid code")
                console.log('error is ', error)
                reject(false);
            }
            console.log("the data : ", data)
            resolve(true);
        }, "get");
    });
}



export let renderTime = (time) => {
    if (!time) return "Time"
    if (time > 1) return time.toFixed(1) + " hrs"
    else return Math.floor(time * 60) + " mins"
}
const args = {
    number: '00442036002013', // String value with the number to call
    prompt: false // Optional boolean property. Determines if the user should be prompt prior to the call 
}
export let callSupport=(phone)=> {
    var s = {
        number: args.number,
        prompt: false,
    };
    if(phone !== "admin"){
        s.number = phone;
    }
    call(s).catch(console.error)
}
export let calculatePrice = (pickup, destination, voucher_id, items, irregularItems, step, config, callback) => {

    if (!config)
        config = {}

    pickup = {
        post_code: pickup.postalCode,
        floor: pickup.floor,
        lift: pickup.lift
    }
    destination = {
        post_code: destination.postalCode,
        floor: destination.floor,
        lift: destination.lift
    }

    switch (step) {
        case "furniture":
            config = {
                help: 0,
                passenger: 0,
                preferred_time: 2,
                include_calendar: false,
                include_help_details: false,
                number_of_days: 7,
                extra_stop: ""
            }
            break;
        case "options":
            config = {
                help: config.help,
                passenger: config.passenger,
                preferred_time: config.preferred_time,
                extra_stop: config.extra_stop,
                include_calendar: false,
                include_help_details: false,
                number_of_days: 1
            }
            break;
        case "calendar":
            config = {
                help: config.help,
                passenger: config.passenger,
                preferred_time: config.preferred_time,
                extra_stop: config.extra_stop,
                include_calendar: true,
                include_help_details: false,
                number_of_days: config.days
            }
            break;
        case "factor":
            config = {
                help: config.help,
                passenger: config.passenger,
                preferred_time: config.preferred_time,
                extra_stop: config.extra_stop,
                include_calendar: false,
                include_help_details: false,
                number_of_days: 1
            }
            break;
        default:
    }

    request("/app/api/v1/price", {
        voucher_id,
        addresses: {
            pickup,
            destination
        },
        items,
        irr_items: irregularItems,

        help: config.help,
        passenger: config.passenger,
        device_id: "",
        preferred_time: config.preferred_time,
        include_help_details: config.include_help_details,
        include_calendar: config.include_calendar,
        extra_stop: config.extra_stop,
        number_of_days: config.number_of_days
    }, callback)
}

export let vanRenderIcons = () =>{
    var results = [Array(26), Array(41), Array(41), Array(41) ];
    results[0][0] = require("../assets/images/vans/0-0.png");
    results[0][1] = require("../assets/images/vans/0-4.png");
    results[0][2] = require("../assets/images/vans/0-8.png");
    results[0][3] = require("../assets/images/vans/0-12.png");
    results[0][4] = require("../assets/images/vans/0-16.png");
    results[0][5] = require("../assets/images/vans/0-20.png");
    results[0][6] = require("../assets/images/vans/0-24.png");
    results[0][7] = require("../assets/images/vans/0-28.png");
    results[0][8] = require("../assets/images/vans/0-32.png");
    results[0][9] = require("../assets/images/vans/0-36.png");
    results[0][10] = require("../assets/images/vans/0-40.png");
    results[0][11] = require("../assets/images/vans/0-44.png");
    results[0][12] = require("../assets/images/vans/0-48.png");
    results[0][13] = require("../assets/images/vans/0-52.png");
    results[0][14] = require("../assets/images/vans/0-56.png");
    results[0][15] = require("../assets/images/vans/0-60.png");
    results[0][16] = require("../assets/images/vans/0-64.png");
    results[0][17] = require("../assets/images/vans/0-68.png");
    results[0][18] = require("../assets/images/vans/0-72.png");
    results[0][19] = require("../assets/images/vans/0-76.png");
    results[0][20] = require("../assets/images/vans/0-80.png");
    results[0][21] = require("../assets/images/vans/0-84.png");
    results[0][22] = require("../assets/images/vans/0-88.png");
    results[0][23] = require("../assets/images/vans/0-92.png");
    results[0][24] = require("../assets/images/vans/0-96.png");
    results[0][25] = require("../assets/images/vans/0-100.png");

    results[1][0] = require("../assets/images/vans/1-0.png");
    results[1][1] = require("../assets/images/vans/1-25.png");
    results[1][2] = require("../assets/images/vans/1-50.png");
    results[1][3] = require("../assets/images/vans/1-75.png");
    results[1][4] = require("../assets/images/vans/1-100.png");
    results[1][5] = require("../assets/images/vans/1-125.png");
    results[1][6] = require("../assets/images/vans/1-150.png");
    results[1][7] = require("../assets/images/vans/1-175.png");
    results[1][8] = require("../assets/images/vans/1-200.png");
    results[1][9] = require("../assets/images/vans/1-225.png");
    results[1][10] = require("../assets/images/vans/1-250.png");
    results[1][11] = require("../assets/images/vans/1-275.png");
    results[1][12] = require("../assets/images/vans/1-300.png");
    results[1][13] = require("../assets/images/vans/1-325.png");
    results[1][14] = require("../assets/images/vans/1-350.png");
    results[1][15] = require("../assets/images/vans/1-375.png");
    results[1][16] = require("../assets/images/vans/1-400.png");
    results[1][17] = require("../assets/images/vans/1-425.png");
    results[1][18] = require("../assets/images/vans/1-450.png");
    results[1][19] = require("../assets/images/vans/1-475.png");
    results[1][20] = require("../assets/images/vans/1-500.png");
    results[1][21] = require("../assets/images/vans/1-525.png");
    results[1][22] = require("../assets/images/vans/1-550.png");
    results[1][23] = require("../assets/images/vans/1-575.png");
    results[1][24] = require("../assets/images/vans/1-600.png");
    results[1][25] = require("../assets/images/vans/1-625.png");
    results[1][26] = require("../assets/images/vans/1-650.png");
    results[1][27] = require("../assets/images/vans/1-675.png");
    results[1][28] = require("../assets/images/vans/1-700.png");
    results[1][29] = require("../assets/images/vans/1-725.png");
    results[1][30] = require("../assets/images/vans/1-750.png");
    results[1][31] = require("../assets/images/vans/1-775.png");
    results[1][32] = require("../assets/images/vans/1-800.png");
    results[1][33] = require("../assets/images/vans/1-825.png");
    results[1][34] = require("../assets/images/vans/1-850.png");
    results[1][35] = require("../assets/images/vans/1-875.png");
    results[1][36] = require("../assets/images/vans/1-900.png");
    results[1][37] = require("../assets/images/vans/1-925.png");
    results[1][38] = require("../assets/images/vans/1-950.png");
    results[1][39] = require("../assets/images/vans/1-975.png");
    results[1][40] = require("../assets/images/vans/1-1000.png");

    results[2][0] = require("../assets/images/vans/2-0.png");
    results[2][1] = require("../assets/images/vans/2-25.png");
    results[2][2] = require("../assets/images/vans/2-50.png");
    results[2][3] = require("../assets/images/vans/2-75.png");
    results[2][4] = require("../assets/images/vans/2-100.png");
    results[2][5] = require("../assets/images/vans/2-125.png");
    results[2][6] = require("../assets/images/vans/2-150.png");
    results[2][7] = require("../assets/images/vans/2-175.png");
    results[2][8] = require("../assets/images/vans/2-200.png");
    results[2][9] = require("../assets/images/vans/2-225.png");
    results[2][10] = require("../assets/images/vans/2-250.png");
    results[2][11] = require("../assets/images/vans/2-275.png");
    results[2][12] = require("../assets/images/vans/2-300.png");
    results[2][13] = require("../assets/images/vans/2-325.png");
    results[2][14] = require("../assets/images/vans/2-350.png");
    results[2][15] = require("../assets/images/vans/2-375.png");
    results[2][16] = require("../assets/images/vans/2-400.png");
    results[2][17] = require("../assets/images/vans/2-425.png");
    results[2][18] = require("../assets/images/vans/2-450.png");
    results[2][19] = require("../assets/images/vans/2-475.png");
    results[2][20] = require("../assets/images/vans/2-500.png");
    results[2][21] = require("../assets/images/vans/2-525.png");
    results[2][22] = require("../assets/images/vans/2-550.png");
    results[2][23] = require("../assets/images/vans/2-575.png");
    results[2][24] = require("../assets/images/vans/2-600.png");
    results[2][25] = require("../assets/images/vans/2-625.png");
    results[2][26] = require("../assets/images/vans/2-650.png");
    results[2][27] = require("../assets/images/vans/2-675.png");
    results[2][28] = require("../assets/images/vans/2-700.png");
    results[2][29] = require("../assets/images/vans/2-725.png");
    results[2][30] = require("../assets/images/vans/2-750.png");
    results[2][31] = require("../assets/images/vans/2-775.png");
    results[2][32] = require("../assets/images/vans/2-800.png");
    results[2][33] = require("../assets/images/vans/2-825.png");
    results[2][34] = require("../assets/images/vans/2-850.png");
    results[2][35] = require("../assets/images/vans/2-875.png");
    results[2][36] = require("../assets/images/vans/2-900.png");
    results[2][37] = require("../assets/images/vans/2-925.png");
    results[2][38] = require("../assets/images/vans/2-950.png");
    results[2][39] = require("../assets/images/vans/2-975.png");
    results[2][40] = require("../assets/images/vans/2-1000.png");

    results[3][0] = require("../assets/images/vans/3-0.png");
    results[3][1] = require("../assets/images/vans/3-25.png");
    results[3][2] = require("../assets/images/vans/3-50.png");
    results[3][3] = require("../assets/images/vans/3-75.png");
    results[3][4] = require("../assets/images/vans/3-100.png");
    results[3][5] = require("../assets/images/vans/3-125.png");
    results[3][6] = require("../assets/images/vans/3-150.png");
    results[3][7] = require("../assets/images/vans/3-175.png");
    results[3][8] = require("../assets/images/vans/3-200.png");
    results[3][9] = require("../assets/images/vans/3-225.png");
    results[3][10] = require("../assets/images/vans/3-250.png");
    results[3][11] = require("../assets/images/vans/3-275.png");
    results[3][12] = require("../assets/images/vans/3-300.png");
    results[3][13] = require("../assets/images/vans/3-325.png");
    results[3][14] = require("../assets/images/vans/3-350.png");
    results[3][15] = require("../assets/images/vans/3-375.png");
    results[3][16] = require("../assets/images/vans/3-400.png");
    results[3][17] = require("../assets/images/vans/3-425.png");
    results[3][18] = require("../assets/images/vans/3-450.png");
    results[3][19] = require("../assets/images/vans/3-475.png");
    results[3][20] = require("../assets/images/vans/3-500.png");
    results[3][21] = require("../assets/images/vans/3-525.png");
    results[3][22] = require("../assets/images/vans/3-550.png");
    results[3][23] = require("../assets/images/vans/3-575.png");
    results[3][24] = require("../assets/images/vans/3-600.png");
    results[3][25] = require("../assets/images/vans/3-625.png");
    results[3][26] = require("../assets/images/vans/3-650.png");
    results[3][27] = require("../assets/images/vans/3-675.png");
    results[3][28] = require("../assets/images/vans/3-700.png");
    results[3][29] = require("../assets/images/vans/3-725.png");
    results[3][30] = require("../assets/images/vans/3-750.png");
    results[3][31] = require("../assets/images/vans/3-775.png");
    results[3][32] = require("../assets/images/vans/3-800.png");
    results[3][33] = require("../assets/images/vans/3-825.png");
    results[3][34] = require("../assets/images/vans/3-850.png");
    results[3][35] = require("../assets/images/vans/3-875.png");
    results[3][36] = require("../assets/images/vans/3-900.png");
    results[3][37] = require("../assets/images/vans/3-925.png");
    results[3][38] = require("../assets/images/vans/3-950.png");
    results[3][39] = require("../assets/images/vans/3-975.png");
    results[3][40] = require("../assets/images/vans/3-1000.png");
    
    return results;
}

