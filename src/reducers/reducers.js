export const voucherReducer = (state = null, action) => {
    switch (action.type) {
        case "SET_VOUCHER_ID":
            return action.payload;
        default:
            return state;
    }
}

export const destinationReducer = (state = null, action) => {
    switch (action.type) {
        case "SET_DESTINATION":
            return Object.assign({},state,action.payload);
        default:
            return state;
    }
}

export const pickupReducer = (state = null, action) => {
    switch (action.type) {
        case "SET_PICKUP":
            return Object.assign({},state,action.payload);
        case "SET_VENDOR_CODE":
            return Object.assign({},state,action.payload);
        default:
            return state;
    }
}

export const itemsReducer = (state = null, action) => {
    switch (action.type) {
        case "SET_ITEMS":
            return Object.assign({},state,action.payload);
        default:
            return state;
    }
}

export const optionsReducer = (state = null, action) => {
    switch (action.type) {
        case "SET_OPTIONS":
            return Object.assign({},state,action.payload);
        default:
            return state;
    }
}

export const scheduleReducer = (state = null, action) => {
    switch (action.type) {
        case "SET_SCHEDULE":
            return Object.assign({},state,action.payload);
        default:
            return state;
    }
}