// Reducers
import { combineReducers } from 'redux';
import { pickupReducer, destinationReducer, voucherReducer,itemsReducer,optionsReducer,scheduleReducer } from './reducers';

const rootReducers = combineReducers({
    pickupReducer,
    destinationReducer,
    voucherReducer,
    itemsReducer,
    optionsReducer,
    scheduleReducer

})

export default rootReducers;