import React from 'react';
import { View, Text, ScrollView, Dimensions, StatusBar, TouchableHighlight, Image, TextInput, TouchableWithoutFeedback, TouchableOpacity } from 'react-native';
import LinearGradient from "react-native-linear-gradient"
import kit from '../assets/styles/kit.js';
import { createIconSetFromIcoMoon } from 'react-native-vector-icons';
import icoMoonConfig from '../assets/icons/selection.json';
import RF from 'react-native-responsive-fontsize';
import styles from '../assets/styles/index.js';
const Icon = createIconSetFromIcoMoon(icoMoonConfig, 'icomoon', 'icomoon.ttf');
const { width, height } = Dimensions.get('window');
import { connect } from 'react-redux';
import Navbar from '../components/navbar.js';
import { GooglePlacesAutocomplete } from '../components/GooglePlacesAutocomplete';
import Submit from '../components/submit.js';
import CircularItem from '../components/circularItem.js';
import { Navigation } from "react-native-navigation";
import { calculatePrice, renderTime, callSupport } from '../tools/index.js';
import BottomCard from "../components/bottomCard"
import { setOptions } from "../actions"

class ShipmentOptions extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            help: 0,
            passengers: 0,
            preferredTime: 2,
            itemsCount: 0,
            extra_stop: "",
            extra_stop_address: "",
            time: null,
            price: null
        }
    }

    clearExtra() {
        this.extraAutoCompleteRef._clearText();
        this.setState({
            extra_stop: "",
            extra_stop_address: "",
        }, () => {
            this.calc();
        });

    }

    findZipCode(address_components) {
        var zip_code = "";
        for (var i = 0; i < address_components.length; i++) {
            if (address_components[i].types[0] === "postal_code") {
                zip_code = address_components[i].short_name;
                break;
            }
        }
        return zip_code;
    }

    changeExtraAddress(details) {
        var zip_code = this.findZipCode(details.address_components);
        this.extraAutoCompleteRef.setAddressText(details.formatted_address);
        this.setState({
            extra_stop: zip_code,
            extra_stop_address: details.formatted_address,
        }, () => { this.calc(); });


    }

    componentDidMount() {
        this.countItems()
        this.calc()
    }
    countItems() {
        let itemsCount = 0;
        this.props.items.map(i => itemsCount += i.number)
        this.props.irr_items.map(i => itemsCount += i.number)
        this.setState({ itemsCount })
    }
    choiceChanged(group, value) {
        if (group === "help") {
            if (value === 2 && this.state.passengers > 1) {
                this.setState({ passengers: 1 });
            } 
            // else if (value === 1 && this.state.passengers > 1) {
            //     this.setState({ passengers: 1 });
            // }
        }

        this.setState({ [group]: value }, () => this.calc())
    }
    calc() {
        calculatePrice(this.props.pickup, this.props.destination, this.props.voucherid, this.props.items, this.props.irr_items, "options", {
            help: this.state.help,
            passenger: this.state.passengers,
            preferred_time: this.state.preferredTime,
            extra_stop: this.state.extra_stop,
        }, (err, result) => {
            if (err) return;
            this.setState({
                time: result.data.time,
                price: result.data.min_price,
            })

        })
    }
    isAdminVoucher() {
        if (String(this.props.voucherid).indexOf("9999") == 0) return true
        return false
    }

    render() {
        return (
            <View style={{ flex: 1, width: '100%', backgroundColor: '#f5f5f5', paddingTop: 70 }} >
                <Navbar
                    leftItem={<Icon onPress={() => Navigation.pop(this.props.componentId)} name="back-arrow-icon" size={25} color="white" />}
                    rightItem={<Icon onPress={() => callSupport(this.props.companyPhone)} name="call-icon-mini" size={25} color="white" />} >

                    <GooglePlacesAutocomplete
                        ref={child => { this.extraAutoCompleteRef = child }}
                        placeholder='[optional] Extra stop address'
                        minLength={3} // minimum length of text to search
                        autoFocus={false}
                        returnKeyType={'search'} // Can be left out for default return key https://facebook.github.io/react-native/docs/textinput.html#returnkeytype
                        listViewDisplayed='false'    // true/false/undefined
                        fetchDetails={true}
                        renderDescription={row => row.description} // custom description render
                        onPress={(data, details = null) => { // 'details' is provided when fetchDetails = true
                            this.changeExtraAddress(details);
                        }}

                        getDefaultValue={() => !this.state.extra_stop_address ? "" : this.state.extra_stop_address}

                        query={{
                            // available options: https://developers.google.com/places/web-service/autocomplete
                            key: env.googleMapsApiKey,
                            language: 'en', // language of the results
                            components: 'country:uk',
                            //types: '(cities)' // default: 'geocode'
                        }}

                        styles={{
                            container: { flex: 1, width: '90%', borderRadius: 15, backgroundColor: '#fff', marginTop: 10 },
                            textInputContainer: {
                                backgroundColor: '#fff',
                                borderRadius: 10,
                                width: "100%",
                                minHeight: RF(7.2),
                                paddingTop: 2
                            },
                            textInput: {
                                paddingLeft: 0,
                                marginLeft: 0,
                                color: kit.colors.inputText,
                                backgroundColor: '#fff',
                            },
                            listView: {
                                maxHeight: RF(20),
                                width: '100%',
                                marginTop: -12,
                                borderBottomLeftRadius: 15,
                                borderBottomRightRadius: 15,
                                backgroundColor: '#fff',
                                flex: 1
                            },
                            description: {
                                color: kit.colors.inputText,
                                fontSize: RF(1.8)
                            },
                            poweredContainer: {
                                display: 'none'
                            },

                            predefinedPlacesDescription: {
                                color: '#1faadb'
                            }
                        }}

                        currentLocation={false} // Will add a 'Current location' button at the top of the predefined places list
                        currentLocationLabel="Current location"
                        nearbyPlacesAPI='GooglePlacesSearch' // Which API to use: GoogleReverseGeocoding or GooglePlacesSearch
                        GoogleReverseGeocodingQuery={{
                            // available options for GoogleReverseGeocoding API : https://developers.google.com/maps/documentation/geocoding/intro
                        }}
                        GooglePlacesSearchQuery={{
                            // available options for GooglePlacesSearch API : https://developers.google.com/places/web-service/search
                            rankby: 'distance',
                            //  types: 'food'
                        }}

                        filterReverseGeocodingByTypes={['locality', 'administrative_area_level_3']} // filter the reverse geocoding results by types - ['locality', 'administrative_area_level_3'] if you want to display only cities
                        // predefinedPlaces={[homePlace, workPlace]}

                        debounce={200} // debounce the requests in ms. Set to 0 to remove debounce. By default 0ms.
                        renderLeftButton={() => <View style={{ height: RF(7.2), marginLeft: 10, marginRight: 5, alignItems: "center", alignSelf: "center", flexDirection: "column", justifyContent: "center" }}>
                            <Icon name="location-icon" size={RF(2.4)} color={kit.colors.icon.dark} />
                        </View>}
                        renderRightButton={() => <View style={{ height: RF(7.2), marginRight: 10, alignItems: "center", alignSelf: "center", justifyContent: "center" }}>
                            <TouchableOpacity onPress={() => { this.clearExtra() }} >
                                <Icon name="clear-textfield-icon" size={RF(2.4)} color={kit.colors.icon.dark} />
                            </TouchableOpacity>
                        </View>}
                    />

                </Navbar>

                <ScrollView
                    contentContainerStyle={{ paddingTop: 45, width: '100%', alignItems: 'center', justifyContent: 'space-between', paddingBottom: height * 0.4 }} >
                    {/* Help Card */}
                    <View style={styles.helpCard} >
                        <View style={styles.helpItem} >
                            <TouchableHighlight
                                underlayColor={'rgba(255,255,255,0.5)'}
                                style={{ height: '100%', width: "30%", borderTopLeftRadius: 15, borderBottomLeftRadius: 15 }}
                                onPress={() => { this.choiceChanged("help", 0) }}>
                                <View style={{ height: '100%', width: "100%", justifyContent: 'center', alignItems: 'center' }} >
                                    <Icon name="no-help-icon" color={this.state.help === 0 ? 'green' : undefined} size={RF(8)} style={{ marginBottom: 0 }} />
                                    {this.state.help === 0 ?
                                        <LinearGradient
                                            start={{ x: 0, y: 1 }} end={{ x: 1, y: 1 }}
                                            // colors={kit.colors.optionsBackground}
                                            colors={kit.colors.submitButton}
                                            style={styles.floorItemUnchecked}>
                                            <Icon name="tick-icon-mini" size={RF(2)} color={"#fff"} />
                                        </LinearGradient> :
                                        <LinearGradient
                                            start={{ x: 0, y: 1 }} end={{ x: 1, y: 1 }}
                                            colors={kit.colors.optionsBackground}
                                            style={styles.floorItemUnchecked} />
                                    }
                                </View>
                            </TouchableHighlight>
                            <View style={{ height: '100%', width: "70%", justifyContent: 'center', paddingHorizontal: 6 }} >
                                <Text style={{ fontSize: RF(2.4) }} >No Help Required</Text>
                                <Text style={{ fontSize: RF(1.7) }} >I can carry my own belongings up and down any stairs</Text>
                            </View>
                        </View>
                        <View style={styles.helpItem} >
                            <TouchableHighlight
                                underlayColor={'rgba(255,255,255,0.5)'}
                                style={{ height: '100%', width: "30%", borderTopLeftRadius: 15, borderBottomLeftRadius: 15 }}
                                onPress={() => { this.choiceChanged("help", 1) }}>
                                <View style={{ height: '100%', width: "100%", justifyContent: 'center', alignItems: 'center' }} >
                                    <Icon name="one-help-icon" color={this.state.help === 1 ? 'green' : undefined} size={RF(8)} style={{ marginBottom: 0 }} />
                                    {this.state.help === 1 ?
                                        <LinearGradient
                                            start={{ x: 0, y: 1 }} end={{ x: 1, y: 1 }}
                                            // colors={kit.colors.optionsBackground}
                                            colors={kit.colors.submitButton}
                                            style={styles.floorItemUnchecked}>
                                            <Icon name="tick-icon-mini" size={RF(2)} color={"#fff"} />
                                        </LinearGradient> :
                                        <LinearGradient
                                            start={{ x: 0, y: 1 }} end={{ x: 1, y: 1 }}
                                            colors={kit.colors.optionsBackground}
                                            style={styles.floorItemUnchecked} />
                                    }
                                </View>
                            </TouchableHighlight>
                            <View style={{ height: '100%', width: "70%", justifyContent: 'center', paddingHorizontal: 6 }} >
                                <Text style={{ fontSize: RF(2.4) }} >Driver to help</Text>
                                <Text style={{ fontSize: RF(1.7) }} >I'd like someone to help and i/we can help with larger items</Text>
                            </View>
                        </View>
                        <View style={styles.helpItem} >
                            <TouchableHighlight
                                underlayColor={'rgba(255,255,255,0.5)'}
                                style={{ height: '100%', width: "30%", borderTopLeftRadius: 15, borderBottomLeftRadius: 15 }}
                                onPress={() => { this.choiceChanged("help", 2) }}>
                                <View style={{ height: '100%', width: "100%", justifyContent: 'center', alignItems: 'center' }} >
                                    <Icon name="help-icon" color={this.state.help === 2 ? 'green' : undefined} size={RF(8)} style={{ marginBottom: 0 }} />
                                    {this.state.help === 2 ?
                                        <LinearGradient
                                            start={{ x: 0, y: 1 }} end={{ x: 1, y: 1 }}
                                            // colors={kit.colors.optionsBackground}
                                            colors={kit.colors.submitButton}
                                            style={styles.floorItemUnchecked}>
                                            <Icon name="tick-icon-mini" size={RF(2)} color={"#fff"} />
                                        </LinearGradient> :
                                        <LinearGradient
                                            start={{ x: 0, y: 1 }} end={{ x: 1, y: 1 }}
                                            colors={kit.colors.optionsBackground}
                                            style={styles.floorItemUnchecked} />
                                    }
                                </View>
                            </TouchableHighlight>
                            <View style={{ height: '100%', width: "70%", justifyContent: 'center', paddingHorizontal: 6 }} >
                                <Text style={{ fontSize: RF(2.4) }} >Driver and mate to help</Text>
                                <Text style={{ fontSize: RF(1.7) }} >We can do everything and two helpers is quicker than one</Text>
                            </View>
                        </View>
                    </View>

                    {/* Passengers card */}
                    <View style={styles.helpCard} >
                        <View style={styles.helpItem} >
                            <TouchableHighlight
                                underlayColor={'rgba(255,255,255,0.5)'}
                                style={{ height: '100%', width: "30%", borderTopLeftRadius: 15, borderBottomLeftRadius: 15 }}
                                onPress={() => { this.choiceChanged("passengers", 0) }}>
                                <View style={{ height: '100%', width: "100%", justifyContent: 'center', alignItems: 'center' }} >
                                    {this.state.passengers === 0 ?
                                        <LinearGradient
                                            start={{ x: 0, y: 1 }} end={{ x: 1, y: 1 }}
                                            colors={kit.colors.submitButton}
                                            style={styles.floorItemUnchecked}>
                                            <Icon name="tick-icon-mini" size={RF(2)} color={"#fff"} />
                                        </LinearGradient> :
                                        <LinearGradient
                                            start={{ x: 0, y: 1 }} end={{ x: 1, y: 1 }}
                                            colors={kit.colors.optionsBackground}
                                            style={styles.floorItemUnchecked} />
                                    }
                                </View>
                            </TouchableHighlight>
                            <View style={{ height: '100%', width: "70%", justifyContent: 'center', paddingHorizontal: 6 }} >
                                <Text style={{ fontSize: RF(2.4) }} >No passengers</Text>
                                <Text style={{ fontSize: RF(1.7) }} >I can make my own travel arrangements</Text>
                            </View>
                        </View>

                        {(this.state.help < 3) && <View style={styles.helpItem} >
                            <TouchableHighlight
                                underlayColor={'rgba(255,255,255,0.5)'}
                                style={{ height: '100%', width: "30%", borderTopLeftRadius: 15, borderBottomLeftRadius: 15 }}
                                onPress={() => { this.choiceChanged("passengers", 1) }}>
                                <View style={{ height: '100%', width: "100%", justifyContent: 'center', alignItems: 'center' }} >
                                    {this.state.passengers === 1 ?
                                        <LinearGradient
                                            start={{ x: 0, y: 1 }} end={{ x: 1, y: 1 }}
                                            colors={kit.colors.submitButton}
                                            style={styles.floorItemUnchecked}>
                                            <Icon name="tick-icon-mini" size={RF(2)} color={"#fff"} />
                                        </LinearGradient> :
                                        <LinearGradient
                                            start={{ x: 0, y: 1 }} end={{ x: 1, y: 1 }}
                                            colors={kit.colors.optionsBackground}
                                            style={styles.floorItemUnchecked} />
                                    }
                                </View>
                            </TouchableHighlight>
                            <View style={{ height: '100%', width: "70%", justifyContent: 'center', paddingHorizontal: 6 }} >
                                <Text style={{ fontSize: RF(2.4) }} >One Passenger</Text>
                                <Text style={{ fontSize: RF(1.7) }} >I'd like to travel in the van</Text>
                            </View>
                        </View>}

                        {(this.state.help < 2) && <View style={styles.helpItem} >
                            <TouchableHighlight
                                underlayColor={'rgba(255,255,255,0.5)'}
                                style={{ height: '100%', width: "30%", borderTopLeftRadius: 15, borderBottomLeftRadius: 15 }}
                                onPress={() => { this.choiceChanged("passengers", 2) }}>
                                <View style={{ height: '100%', width: "100%", justifyContent: 'center', alignItems: 'center' }} >
                                    {this.state.passengers === 2 ?
                                        <LinearGradient
                                            start={{ x: 0, y: 1 }} end={{ x: 1, y: 1 }}
                                            colors={kit.colors.submitButton}
                                            style={styles.floorItemUnchecked}>
                                            <Icon name="tick-icon-mini" size={RF(2)} color={"#fff"} />
                                        </LinearGradient> :
                                        <LinearGradient
                                            start={{ x: 0, y: 1 }} end={{ x: 1, y: 1 }}
                                            colors={kit.colors.optionsBackground}
                                            style={styles.floorItemUnchecked} />
                                    }
                                </View>
                            </TouchableHighlight>
                            <View style={{ height: '100%', width: "70%", justifyContent: 'center', paddingHorizontal: 6 }} >
                                <Text style={{ fontSize: RF(2.4) }} >Two Passengers</Text>
                                <Text style={{ fontSize: RF(1.7) }} >We need 2 spaces in the van</Text>
                            </View>
                        </View>}

                        <View style={styles.helpItemFooter} >
                            <View style={{ height: '100%', width: "100%", justifyContent: 'center', paddingHorizontal: 6 }} >
                                <Text style={{ fontSize: RF(1.7) }} >No pets or children in the cab unless agreed in advance</Text>
                            </View>
                        </View>
                    </View>
                    {/* preferredTime card */}
                    <View style={[styles.helpCard]} >

                        <View style={styles.helpItem} >
                            <TouchableHighlight
                                underlayColor={'rgba(255,255,255,0.5)'}
                                style={{ height: '100%', width: "30%", borderTopLeftRadius: 15, borderBottomLeftRadius: 15 }}
                                onPress={() => { this.choiceChanged("preferredTime", 2) }}>
                                <View style={{ height: '100%', width: "100%", justifyContent: 'center', alignItems: 'center' }} >
                                    {this.state.preferredTime === 2 ?
                                        <LinearGradient
                                            start={{ x: 0, y: 1 }} end={{ x: 1, y: 1 }}
                                            // colors={kit.colors.optionsBackground}
                                            colors={kit.colors.submitButton}
                                            style={styles.floorItemUnchecked}>
                                            <Icon name="tick-icon-mini" size={RF(2)} color={"#fff"} />
                                        </LinearGradient> :
                                        <LinearGradient
                                            start={{ x: 0, y: 1 }} end={{ x: 1, y: 1 }}
                                            colors={kit.colors.optionsBackground}
                                            style={styles.floorItemUnchecked} />
                                    }
                                </View>
                            </TouchableHighlight>
                            <View style={{ height: '100%', width: "70%", justifyContent: 'center', paddingHorizontal: 6 }} >
                                <Text style={{ fontSize: RF(2.4) }} >Morning 8am to Midday start</Text>
                                <Text style={{ fontSize: RF(1.7) }} >Most popular but there can be limited time slots available</Text>
                            </View>
                        </View>
                        <View style={styles.helpItem} >
                            <TouchableHighlight
                                underlayColor={'rgba(255,255,255,0.5)'}
                                style={{ height: '100%', width: "30%", borderTopLeftRadius: 15, borderBottomLeftRadius: 15 }}
                                onPress={() => { this.choiceChanged("preferredTime", 3) }}>
                                <View style={{ height: '100%', width: "100%", justifyContent: 'center', alignItems: 'center' }} >
                                    {this.state.preferredTime === 3 ?
                                        <LinearGradient
                                            start={{ x: 0, y: 1 }} end={{ x: 1, y: 1 }}
                                            // colors={kit.colors.optionsBackground}
                                            colors={kit.colors.submitButton}
                                            style={styles.floorItemUnchecked}>
                                            <Icon name="tick-icon-mini" size={RF(2)} color={"#fff"} />
                                        </LinearGradient> :
                                        <LinearGradient
                                            start={{ x: 0, y: 1 }} end={{ x: 1, y: 1 }}
                                            colors={kit.colors.optionsBackground}
                                            style={styles.floorItemUnchecked} />
                                    }
                                </View>
                            </TouchableHighlight>
                            <View style={{ height: '100%', width: "70%", justifyContent: 'center', paddingHorizontal: 6 }} >
                                <Text style={{ fontSize: RF(2.4) }} >Afternoon 1pm to 5pm start</Text>
                                <Text style={{ fontSize: RF(1.7) }} >Not so busy and sometimes better prices</Text>
                            </View>
                        </View>

                        <View style={styles.helpItem} >
                            <TouchableHighlight
                                underlayColor={'rgba(255,255,255,0.5)'}
                                style={{ height: '100%', width: "30%", borderTopLeftRadius: 15, borderBottomLeftRadius: 15 }}
                                onPress={() => { this.choiceChanged("preferredTime", 1) }}>
                                <View style={{ height: '100%', width: "100%", justifyContent: 'center', alignItems: 'center' }} >
                                    {this.state.preferredTime === 1 ?
                                        <LinearGradient
                                            start={{ x: 0, y: 1 }} end={{ x: 1, y: 1 }}
                                            // colors={kit.colors.optionsBackground}
                                            colors={kit.colors.submitButton}
                                            style={styles.floorItemUnchecked}>
                                            <Icon name="tick-icon-mini" size={RF(2)} color={"#fff"} />
                                        </LinearGradient> :
                                        <LinearGradient
                                            start={{ x: 0, y: 1 }} end={{ x: 1, y: 1 }}
                                            colors={kit.colors.optionsBackground}
                                            style={styles.floorItemUnchecked} />
                                    }
                                </View>
                            </TouchableHighlight>
                            <View style={{ height: '100%', width: "70%", justifyContent: 'center', paddingHorizontal: 6 }} >
                                <Text style={{ fontSize: RF(2.4) }} >Early Mornings before 8am start</Text>
                                <Text style={{ fontSize: RF(1.7) }} >Ideal for short days and long journeys</Text>
                            </View>
                        </View>


                        <View style={styles.helpItem} >
                            <TouchableHighlight
                                underlayColor={'rgba(255,255,255,0.5)'}
                                style={{ height: '100%', width: "30%", borderTopLeftRadius: 15, borderBottomLeftRadius: 15 }}
                                onPress={() => { this.choiceChanged("preferredTime", 4) }}>
                                <View style={{ height: '100%', width: "100%", justifyContent: 'center', alignItems: 'center' }} >
                                    {this.state.preferredTime === 4 ?
                                        <LinearGradient
                                            start={{ x: 0, y: 1 }} end={{ x: 1, y: 1 }}
                                            // colors={kit.colors.optionsBackground}
                                            colors={kit.colors.submitButton}
                                            style={styles.floorItemUnchecked}>
                                            <Icon name="tick-icon-mini" size={RF(2)} color={"#fff"} />
                                        </LinearGradient> :
                                        <LinearGradient
                                            start={{ x: 0, y: 1 }} end={{ x: 1, y: 1 }}
                                            colors={kit.colors.optionsBackground}
                                            style={styles.floorItemUnchecked} />
                                    }
                                </View>
                            </TouchableHighlight>
                            <View style={{ height: '100%', width: "70%", justifyContent: 'center', paddingHorizontal: 6 }} >
                                <Text style={{ fontSize: RF(2.4) }} >Evenings starting after 5pm</Text>
                                <Text style={{ fontSize: RF(1.7) }} >Great for juggling other commitments</Text>
                            </View>
                        </View>
                        <View style={styles.helpItemFooter} >
                            <View style={{ height: '100%', width: "100%", justifyContent: 'center', paddingHorizontal: 6 }} >
                                <Text style={{ fontSize: RF(1.7) }} >We will contact you for exact times upon booking</Text>
                            </View>
                        </View>
                    </View>


                </ScrollView>
                <BottomCard body={
                    <View style={{ flexDirection: "row", justifyContent: "space-between" }}>
                        <CircularItem text={this.state.itemsCount + " Items"} icon="items-icon" />
                        <CircularItem text={renderTime(this.state.time)} icon="time-icon" />
                        <CircularItem text={(this.state.price) ? "From £" + this.state.price : "Min Price"} icon="price-icon" />
                    </View>
                }
                    action={
                        <Submit
                            icon="next-arrow-icon"
                            onPress={() => {
                                this.props.setOptions({
                                    help: this.state.help,
                                    preferred_time: this.state.preferredTime,
                                    passenger: this.state.passengers,
                                    extra_stop: this.state.extra_stop,
                                    extra_stop_address: this.state.extra_stop_address
                                })
                                Navigation.push(this.props.componentId, { component: { name: "CompaniesSchedule", id: "CompaniesSchedule" } })
                            }}
                            text={"Next"}
                            enabled />

                    } />
            </View>
        )
    }
}

const mapStateToProps = state => {
    return {
        pickup: state.pickupReducer.pickup,
        destination: state.destinationReducer.destination,
        voucherid: state.voucherReducer.voucherid,
        items: state.itemsReducer.items,
        irr_items: state.itemsReducer.irr_items,
        companyPhone: state.itemsReducer.companyPhone
    }
}

export default connect(mapStateToProps, { setOptions })(ShipmentOptions);