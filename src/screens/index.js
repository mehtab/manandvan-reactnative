import React from 'react';
import { Navigation } from 'react-native-navigation';
import { Provider } from 'react-redux';
import { configureStore, sagaMiddleware } from '../store';
import sagas from '../sagas';

//SCREENS
import Home from './Home';
import CompaniesSchedule from './CompaniesSchedule'
import ShipmentOptions from './ShipmentOptions';
import ShipmentSummary from './ShipmentSummary';
import SelectFurniture from './SelectFurniture';

let store = configureStore();


let reduxStoreWrapper = (MyComponent, store) => {
    return () => {
        return class StoreWrapper extends React.Component {
            render() {
                return (
                    <Provider store={store}>
                        <MyComponent {...this.props} />
                    </Provider>
                );
            }
        };
    };
}

export function registerScreens() {
    // Navigation.registerComponent('firstScreen', reduxStoreWrapper(, store));
    Navigation.registerComponent('home', reduxStoreWrapper(Home, store));
    Navigation.registerComponent('CompaniesSchedule', reduxStoreWrapper(CompaniesSchedule, store));
    Navigation.registerComponent('ShipmentOptions', reduxStoreWrapper(ShipmentOptions, store));
    Navigation.registerComponent('ShipmentSummary', reduxStoreWrapper(ShipmentSummary, store));
    Navigation.registerComponent('SelectFurniture', reduxStoreWrapper(SelectFurniture, store));
}

sagaMiddleware.run(sagas)