import React, { createRef } from 'react';
import {
    View, Text, TextInput,
    TouchableOpacity, Dimensions,
    Animated, Image, Keyboard, Modal
}
    from 'react-native';
import MapView from 'react-native-maps'
import { Navigation } from "react-native-navigation";
import { connect } from 'react-redux'
import kit from '../assets/styles/kit.js';
import { createIconSetFromIcoMoon } from 'react-native-vector-icons';
import icoMoonConfig from '../assets/icons/selection.json';
import RF from 'react-native-responsive-fontsize';
import FloorMenu from '../components/floorMenu.js';
import Navbar from "../components/navbar"

import CircularItem from "../components/circularItem"
import { GooglePlacesAutocomplete } from '../components/GooglePlacesAutocomplete';
import Polyline from '@mapbox/polyline';
import { setPickup, setDestination, setVoucher, setVendorCode } from '../actions';

import styles from "../assets/styles"
import Submit from '../components/submit.js';
import VoucherModal from '../components/voucherModal';
import { callSupport, verifyVendorCode } from "../tools"
import BottomCard from "../components/bottomCard"
import GetVendorCode from '../components/GetVendorCode.js';
const { width, height } = Dimensions.get('window');


const Icon = createIconSetFromIcoMoon(icoMoonConfig, 'icomoon', 'icomoon.ttf');

class Home extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            vendorCode: '',
            validVendorCode: '',
            modelVisible: true,
            from: null,
            fromZipcode: null,
            to: null,
            toZipcode: null,


            Regionlatitude: 51.507407,
            Regionlongitude: -0.127763,
            RegionlatitudeDelta: 0.005,
            RegionlongitudeDelta: 0.007,
            fromCoord: { latitude: 51.507407, longitude: -0.127763 },
            fromChoosed: false,
            FromPinSize: new Animated.Value(37),

            toCoord: { latitude: 51.507407, longitude: -0.127763 },
            toChoosed: false,
            toPinSize: new Animated.Value(37),

            fromFloor: null,
            toFloor: null,

            openFromFloors: false,
            openToFloors: false,

            fromLift: false,
            toLift: false,
            openVoucher: false,
            coords: [],
            bottomItemsOpen: true,

            fromError: false,
            toError: false,

            callUs: false,
        };


    }
    onRegionChange(region) {
        if (Math.abs(this.state.Regionlatitude - region.latitude) < 0.0001 && Math.abs(this.state.Regionlongitude - region.longitude) < 0.00001) return
        this.setState({ Regionlatitude: region.latitude, Regionlongitude: region.longitude, RegionlatitudeDelta: region.latitudeDelta, RegionlongitudeDelta: region.longitudeDelta });
    }

    generatePostalCode(lat, lng, callback) {
        this.addressApi(`${lat},${lng}`, callback)
    }

    findZipCode(address_components) {
        var zip_code = "";
        for (var i = 0; i < address_components.length; i++) {
            if (address_components[i].types[0] === "postal_code") {
                zip_code = address_components[i].short_name;
                break;
            }
        }
        return zip_code;
    }

    addressApi(address, callback) {
        fetch('https://maps.googleapis.com/maps/api/geocode/json?address=' + address + '&key=' + env.googleMapsApiKey)
            .then((response) => response.json())
            .then((responseJson) => {

                var full_address = responseJson.results[0];
                var formatted_address = full_address.formatted_address;
                var zip_code = this.findZipCode(full_address.address_components);
                if (zip_code === "") {
                    return callback("Failed")
                }
                callback(null, { address: formatted_address, zipCode: zip_code, location: full_address.geometry.location })
            }).catch(e => {
                callback(e)
            })
    }

    componentDidMount() {
        this.getUserPosition();
    }




    async submitCode() {

        const code = this.state.vendorCode

        if (code === '') {
            alert("Enter code")
            return
        }


        const data = {
            supplier_id: code
        }

        const v = await verifyVendorCode(data)

        if (v) {
            this.props.setVendorCode({
                validVendorCode: code
            })

            this.setState({
                validVendorCode: code,
                modelVisible: false
            })
        }
        else {
            alert("Invalid Code")
        }

        this.setState({
            vendorCode: ''
        })
    }

    getUserPosition = () => {
        navigator.geolocation.getCurrentPosition(
            (position) => {
                this._map.animateToRegion({
                    latitude: position.coords.latitude,
                    longitude: position.coords.longitude,
                    longitudeDelta: this.state.RegionlongitudeDelta,
                    latitudeDelta: this.state.RegionlatitudeDelta,
                }, 1000)

                this.setState({
                    Regionlatitude: position.coords.latitude,
                    Regionlongitude: position.coords.longitude,
                });
            },
            (error) => { /*alert(error)*/ },
            { enableHighAccuracy: false, timeout: 20000, maximumAge: 1000 },
        );
    }

    clearFrom() {
        this.fromAutoCompleteRef._clearText();
        this.setState({
            from: null,
            fromZipcode: null,
            fromCoord: { latitude: this.state.Regionlatitude, longitude: this.state.Regionlongitude },
            fromChoosed: false,
            coords: []
        });

    }

    chooseFrom(lat, long) {
        this.setState({
            fromCoord: { latitude: lat, longitude: long },
            Regionlatitude: this.state.toCoord.latitude,
            Regionlongitude: this.state.toCoord.longitude,
            fromChoosed: true
        }, () => {

            this.generatePostalCode(lat, long, (err, resolved) => {
                if (err) {
                    this.setState({ from: "UNKNOWN" });
                    return;
                }
                this.fromAutoCompleteRef.setAddressText(resolved.address);
                this.setState({ from: resolved.address, fromZipcode: resolved.zipCode });
                this.showPath();
            })
        });

    }

    async getDirections(startLoc, destinationLoc) {

        try {
            startLoc = startLoc.latitude + "," + startLoc.longitude
            destinationLoc = destinationLoc.latitude + "," + destinationLoc.longitude
            let resp = await fetch(`https://maps.googleapis.com/maps/api/directions/json?origin=${startLoc}&destination=${destinationLoc}&key=${env.googleMapsApiKey}`)
            let respJson = await resp.json();
            let points = Polyline.decode(respJson.routes[0].overview_polyline.points);
            let coords = points.map((point, index) => {
                return {
                    latitude: point[0],
                    longitude: point[1]
                }
            })
            this.setState({ coords: coords })
            return coords
        } catch (error) {
            // alert(error)
            return error
        }
    }

    chooseTo(lat, long) {
        this.setState({
            toCoord: { latitude: lat, longitude: long },
            Regionlatitude: lat,
            Regionlongitude: long,
            toChoosed: true
        }, () => {
            // update input text
            this.generatePostalCode(lat, long, (err, resolved) => {
                if (err) {
                    this.setState({ to: "UNKNOWN" })
                    return;
                }
                this.toAutoCompleteRef.setAddressText(resolved.address);
                this.setState({
                    to: resolved.address,
                    toZipcode: resolved.zipCode
                });
                this.showPath();
            })
        });
    }

    clearTo() {
        this.toAutoCompleteRef._clearText();
        this.setState({ toZipcode: null, to: null, toCoord: { latitude: this.state.Regionlatitude, longitude: this.state.Regionlongitude }, toChoosed: false, coords: [] });
    }

    voucherSubmitted(voucher) {
        let { fromCoord, fromFloor, fromLift, fromZipcode,
            toCoord, toFloor, toLift, toZipcode, from, to } = this.state;
        this.setState({ voucher })
        this.props.setPickup({
            pickup: {
                coord: fromCoord,
                postalCode: fromZipcode,
                lift: fromLift,
                floor: fromFloor,
                fullAddress: from
            }
        })
        this.props.setDestination({
            destination: {
                coord: toCoord,
                postalCode: toZipcode,
                lift: toLift,
                floor: toFloor,
                fullAddress: to
            }
        })
        this.setState({ openVoucher: false, })
        this.props.setVoucher({ voucherid: voucher })
        Navigation.push(this.props.componentId, { component: { name: "SelectFurniture", id: "SelectFurniture" } })
    }

    renderFloorName(floor) {
        var floorName = "";
        switch (floor) {
            case -1:
                floorName = "Basement"
                break;
            case 0:
                floorName = "Ground"
                break;
            case 1:
                floorName = "1st floor"
                break;
            case 2:
                floorName = "2nd floor"
                break;
            case 3:
                floorName = "3rd floor"
                break;
            case 4:
                floorName = "4th floor"
                break;
            case 5:
                floorName = "5th+ floor"
                break;
            default:
                floorName = "floor";
                break;
        }
        return floorName;
    }

    toggleLift(step) {
        if (step == 0) {
            this.setState({ fromLift: !this.state.fromLift, callUs: this.state.fromFloor === 5 && this.state.fromLift ? true : false })
        }
        else if (step >= 1) this.setState({ toLift: !this.state.toLift, callUs: this.state.toFloor === 5 && this.state.toLift ? true : false })
    }
    liftColor(step) {
        if (step == 0) {
            if (this.state.fromLift) return "#00c853"
            else return "#ff7043"
        }
        else {
            if (this.state.toLift) return "#00c853"
            else return "#ff7043"
        }
    }
    liftText(step) {
        if (step == 0) {
            if (this.state.fromLift) return "Has lift"
            else return "No lift"
        }
        else {
            if (this.state.toLift) return "Has lift"
            else return "No lift"
        }
    }

    showPath() {
        if (!this.state.fromChoosed || !this.state.toChoosed) {
            this.setState({ coords: [] });
            return;
        }

        this.getDirections(this.state.fromCoord, this.state.toCoord)
        var centerLat = (this.state.fromCoord.latitude + this.state.toCoord.latitude) / 2;
        var diffLat = Math.abs(centerLat - this.state.fromCoord.latitude);
        var upPoint = centerLat + diffLat;
        var downPoint = centerLat - diffLat;

        this._map.fitToCoordinates([
            { latitude: upPoint, longitude: this.state.fromCoord.longitude },
            { latitude: downPoint, longitude: this.state.toCoord.longitude }], { edgePadding: { top: height * 0.1, right: width * 0.1, bottom: height * 0.1, left: width * 0.1 }, animated: true });

    }

    submit() {
        this.setState({ openVoucher: true })
    }

    openFloorModal(step) {
        this.setState({ openFromFloors: (step == 0), openToFloors: (step > 0) })
    }

    changeFromAddress(details) {
        // console.log(details.geometry.location);

        this._map.animateToRegion({
            latitude: details.geometry.location.lat,
            longitude: details.geometry.location.lng,
            latitudeDelta: 0.001,
            longitudeDelta: 0.001
        }, 100)
        var zip_code = this.findZipCode(details.address_components);
        this.setState({
            from: details.formatted_address,
            fromCoord: {
                latitude: details.geometry.location.lat,
                longitude: details.geometry.location.lng,
            },
            fromChoosed: true,
            fromZipcode: zip_code,
            region: {
                latitude: details.geometry.location.lat,
                longitude: details.geometry.location.lng,
                latitudeDelta: 0.001,
                longitudeDelta: 0.001
            }
        });
        this.showPath();
    }

    onChangeAddress(fromto) {
        this.state.fromChanging = false;
        this.state.toChanging = false;
        if (fromto === "from") {
            this.setState({ fromChanging: true })
        }
        else if (fromto === "to") {
            this.setState({ toChanging: true })
        }
    }

    changeToAddress(details) {
        this._map.animateToRegion({
            latitude: details.geometry.location.lat,
            longitude: details.geometry.location.lng,
            latitudeDelta: 0.001,
            longitudeDelta: 0.001
        }, 100)
        var zip_code = this.findZipCode(details.address_components);
        this.setState({
            to: details.formatted_address,
            toCoord: {
                latitude: details.geometry.location.lat,
                longitude: details.geometry.location.lng,
            },
            toChoosed: true,
            toZipcode: zip_code,
            region: {
                latitude: details.geometry.location.lat,
                longitude: details.geometry.location.lng,
                latitudeDelta: 0.001,
                longitudeDelta: 0.001
            }
        });
        this.showPath();
    }

    nextButtonText() {
        if (this.state.callUs) {
            return "Call us";
        }
        if (!this.state.fromChoosed) {
            return "Select pickup address";
        }
        if (this.state.fromFloor === null) {
            return "pickup floor and lift";
        }
        if (!this.state.toChoosed) {
            return "Select destination address";
        }
        if (this.state.toFloor === null) {
            return "destination floor and lift";
        }
        return "Next";

    }

    nextButtonState() {
        if (this.state.callUs) {
            return true;
        }
        return (this.state.fromChoosed && this.state.toChoosed && this.state.fromFloor !== null && this.state.toFloor !== null);
    }

    render() {
        let code = this.state.validVendorCode

        return (
            <View style={{
                flex: 1, marginTop: 0, height: "100%",
                justifyContent: "center", alignItems: 'center'
            }}>
                <Modal
                    visible={this.state.modelVisible} onRequestClose={() => { alert("modal closed") }}>
                    <View style={{
                        width: '70%',
                        flex: 1,
                        backgroundColor: 'white',
                        justifyContent: 'center',
                        alignSelf: 'center'


                    }}>
                        <TextInput
                            placeholder='Please enter the vendor code here'
                            style={{
                                borderWidth: 1,
                                borderRadius: 10,
                                borderColor: 'grey'
                            }}
                            autoFocus={true}
                            value={this.state.vendorCode}
                            onChangeText={(code) => this.setState({
                                vendorCode: code
                            })}
                        />
                        <Submit
                            onPress={() => this.submitCode()}
                            text={"SUBMIT"}
                            enabled={true} />

                    </View>

                </Modal>
                <MapView ref={(component) => this._map = component} initialRegion={{
                    latitude: 51.507407,
                    longitude: -0.127763,
                    latitudeDelta: 0.05,
                    longitudeDelta: 0.07,
                }}
                    style={{ width: '100%', flex: 1 }}
                    customMapStyle={require('../tools/mapStyle.json')}
                    onRegionChangeComplete={(region) => this.onRegionChange(region)}
                >
                    <MapView.Polyline
                        coordinates={this.state.coords}
                        strokeWidth={5}
                        strokeColor="#1EE39E" />

                    {this.state.fromChoosed && <MapView.Marker onPress={() => (this.state.fromChoosed == false) ? this.chooseFrom(this.state.Regionlatitude, this.state.Regionlongitude) : this.clearFrom()} style={{ paddingTop: 20 }} anchor={{ x: 0.5, y: 0.7 }} coordinate={this.state.fromCoord} >
                        <Image source={require("../assets/icons/from.png")} style={{ width: 27, maxHeight: 27 }} resizeMode="contain" />
                    </MapView.Marker>}

                    {this.state.toChoosed &&
                        <MapView.Marker onPress={() => (this.state.toChoosed == false) ? this.chooseTo(this.state.Regionlatitude, this.state.Regionlongitude) : this.clearTo()} anchor={{ x: 0.5, y: 0.7 }} style={{ paddingTop: 20 }} coordinate={this.state.toCoord} >
                            <Image source={require("../assets/icons/to.png")} style={{ width: 27, maxHeight: 27 }} resizeMode="contain" />
                        </MapView.Marker>}
                </MapView>
                {/* {!this.state.fromChoosed &&
                    <TouchableOpacity
                        style={{ position: "absolute", alignSelf: "center", padding: 20 }}
                        onPress={() => (this.state.fromChoosed == false) ? this.chooseFrom(this.state.Regionlatitude, this.state.Regionlongitude) : this.clearFrom()} >
                        <Image source={require("../assets/icons/from.png")} style={{ width: 37, maxHeight: 37 }} resizeMode="contain" />
                    </TouchableOpacity>
                }
                {(!this.state.toChoosed) &&
                    <TouchableOpacity style={{ position: "absolute", alignSelf: "center", padding: 20 }} onPress={() => (this.state.toChoosed == false) ? this.chooseTo(this.state.Regionlatitude, this.state.Regionlongitude) : this.clearTo()} >
                        <Image source={require("../assets/icons/to.png")} style={{ width: 37, maxHeight: 37 }} resizeMode="contain" />
                    </TouchableOpacity>
                } */}

                <BottomCard
                    action={
                        <Submit
                            icon={this.state.callUs ? "call-icon-mini" : "next-arrow-icon"}
                            onPress={() => {
                                if (this.state.callUs) {
                                    callSupport("admin")
                                } else {
                                    this.voucherSubmitted("99990001");
                                }
                            }}
                            text={this.nextButtonText()}
                            enabled={this.nextButtonState()} />
                    } />
                {/* navbar */}
                <Navbar rightItem={<View />
                } leftItem={
                    <TouchableOpacity onPress={() => callSupport("admin")}>
                        <Icon name="call-icon-mini" size={22} color="white" />
                    </TouchableOpacity>
                } hideTopBar={true}>

                    {/* from field */}
                    <View style={{ width: "100%", marginTop: 5, justifyContent: "flex-start", alignItems: "flex-start", flexDirection: "row" }} >

                        <GooglePlacesAutocomplete
                            ref={child => { this.fromAutoCompleteRef = child }}
                            placeholder='Pickup address'
                            minLength={3} // minimum length of text to search
                            autoFocus={false}
                            returnKeyType={'search'} // Can be left out for default return key https://facebook.github.io/react-native/docs/textinput.html#returnkeytype
                            listViewDisplayed='false'    // true/false/undefined
                            fetchDetails={true}
                            renderDescription={row => row.description} // custom description render
                            onPress={(data, details = null) => { // 'details' is provided when fetchDetails = true
                                console.log('pressed', details);
                                this.changeFromAddress(details);
                            }}

                            getDefaultValue={() => !this.state.from ? "" : this.state.from}

                            query={{
                                // available options: https://developers.google.com/places/web-service/autocomplete
                                key: env.googleMapsApiKey,
                                language: 'en', // language of the results
                                components: 'country:uk',
                                //types: '(cities)' // default: 'geocode'
                            }}

                            styles={{
                                textInputContainer: {
                                    backgroundColor: kit.colors.inputBackground,
                                    borderRadius: 10,
                                    width: "100%",
                                    minHeight: RF(7.2),
                                    paddingTop: 0
                                },
                                textInput: {
                                    paddingLeft: 0,
                                    marginLeft: 0,
                                    color: kit.colors.inputText,
                                    backgroundColor: kit.colors.inputBackground
                                },
                                listView: {
                                    maxHeight: RF(20),
                                    width: '100%',
                                    marginTop: -12,
                                    borderBottomLeftRadius: 15,
                                    borderBottomRightRadius: 15,
                                    backgroundColor: kit.colors.inputBackground,
                                },
                                description: {
                                    color: kit.colors.inputText,
                                    fontSize: RF(1.8)
                                },
                                poweredContainer: {
                                    display: 'none'
                                },

                                predefinedPlacesDescription: {
                                    color: '#1faadb'
                                }
                            }}

                            currentLocation={false} // Will add a 'Current location' button at the top of the predefined places list
                            currentLocationLabel="Current location"
                            nearbyPlacesAPI='GooglePlacesSearch' // Which API to use: GoogleReverseGeocoding or GooglePlacesSearch
                            GoogleReverseGeocodingQuery={{
                                // available options for GoogleReverseGeocoding API : https://developers.google.com/maps/documentation/geocoding/intro
                            }}
                            GooglePlacesSearchQuery={{
                                // available options for GooglePlacesSearch API : https://developers.google.com/places/web-service/search
                                rankby: 'distance',
                                //  types: 'food'
                            }}

                            filterReverseGeocodingByTypes={['locality', 'administrative_area_level_3']} // filter the reverse geocoding results by types - ['locality', 'administrative_area_level_3'] if you want to display only cities
                            // predefinedPlaces={[homePlace, workPlace]}

                            debounce={200} // debounce the requests in ms. Set to 0 to remove debounce. By default 0ms.
                            renderLeftButton={() => <View style={{ height: RF(7.2), marginLeft: 10, marginRight: 5, alignItems: "center", alignSelf: "center", flexDirection: "column", justifyContent: "center" }}>
                                <Icon name="location-icon" size={RF(2.4)} color={(this.state.fromError) ? "#ff7043" : kit.colors.icon.dark} />
                            </View>}
                            renderRightButton={() => <View style={{ height: RF(7.2), marginRight: 10, alignItems: "center", alignSelf: "center", justifyContent: "center" }}>
                                <TouchableOpacity onPress={() => { this.clearFrom() }} >
                                    <Icon name="clear-textfield-icon" size={RF(2.4)} color={(this.state.fromError) ? "#ff7043" : kit.colors.icon.dark} />
                                </TouchableOpacity>
                            </View>}
                        />

                        <View style={{ width: "20%", marginLeft: 10, flexDirection: "row", justifyContent: "space-between", marginRight: 15 }}>
                            <CircularItem type={'small'} text={this.renderFloorName(this.state.fromFloor)} icon="home-icon" onPress={() => this.openFloorModal(0)} />
                            <CircularItem type={'small'} text={this.liftText(0)} icon="lift-active-icon" color={this.liftColor(0)} onPress={() => this.toggleLift(0)} />
                        </View>
                    </View>

                    {/* to field */}
                    <View style={{ width: "100%", marginTop: 5, justifyContent: "flex-start", alignItems: "flex-start", flexDirection: "row" }} >

                        <GooglePlacesAutocomplete
                            ref={child => { this.toAutoCompleteRef = child }}
                            placeholder='Destination address'
                            minLength={4} // minimum length of text to search
                            autoFocus={false}
                            returnKeyType={'search'} // Can be left out for default return key https://facebook.github.io/react-native/docs/textinput.html#returnkeytype
                            listViewDisplayed='false'    // true/false/undefined
                            fetchDetails={true}
                            renderDescription={row => row.description} // custom description render
                            onPress={(data, details = null) => { // 'details' is provided when fetchDetails = true
                                this.changeToAddress(details);
                            }}

                            getDefaultValue={() => !this.state.to ? "" : this.state.to}

                            query={{
                                // available options: https://developers.google.com/places/web-service/autocomplete
                                key: env.googleMapsApiKey,
                                language: 'en', // language of the results
                                components: 'country:uk',
                                //types: '(cities)' // default: 'geocode'
                            }}

                            styles={{
                                textInputContainer: {
                                    backgroundColor: kit.colors.inputBackground,
                                    borderRadius: 10,
                                    width: "100%",
                                    minHeight: RF(7.2),
                                    paddingTop: 2
                                },
                                textInput: {
                                    paddingLeft: 0,
                                    marginLeft: 0,
                                    color: kit.colors.inputText,
                                    backgroundColor: kit.colors.inputBackground
                                },
                                listView: {
                                    maxHeight: RF(20),
                                    width: '100%',
                                    marginTop: -12,
                                    borderBottomLeftRadius: 15,
                                    borderBottomRightRadius: 15,
                                    backgroundColor: kit.colors.inputBackground,
                                },
                                description: {
                                    color: kit.colors.inputText,
                                    fontSize: RF(1.8)
                                },
                                poweredContainer: {
                                    display: 'none'
                                },

                                predefinedPlacesDescription: {
                                    color: '#1faadb'
                                }
                            }}

                            currentLocation={false} // Will add a 'Current location' button at the top of the predefined places list
                            currentLocationLabel="Current location"
                            nearbyPlacesAPI='GooglePlacesSearch' // Which API to use: GoogleReverseGeocoding or GooglePlacesSearch
                            GoogleReverseGeocodingQuery={{
                                // available options for GoogleReverseGeocoding API : https://developers.google.com/maps/documentation/geocoding/intro
                            }}
                            GooglePlacesSearchQuery={{
                                // available options for GooglePlacesSearch API : https://developers.google.com/places/web-service/search
                                rankby: 'distance',
                                //    types: 'food'
                            }}

                            filterReverseGeocodingByTypes={['locality', 'administrative_area_level_3']} // filter the reverse geocoding results by types - ['locality', 'administrative_area_level_3'] if you want to display only cities
                            // predefinedPlaces={[homePlace, workPlace]}

                            debounce={200} // debounce the requests in ms. Set to 0 to remove debounce. By default 0ms.
                            renderLeftButton={() => <View style={{ height: RF(7.2), marginLeft: 10, marginRight: 5, alignItems: "center", alignSelf: "center", flexDirection: "column", justifyContent: "center" }}>
                                <Icon name="destination-icon" size={RF(2.4)} color={(this.state.toError) ? "#ff7043" : kit.colors.icon.dark} />
                            </View>}
                            renderRightButton={() => <View style={{ height: RF(7.2), marginRight: 10, alignItems: "center", alignSelf: "center", justifyContent: "center" }}>
                                <TouchableOpacity onPress={() => this.clearTo()} >
                                    <Icon name="clear-textfield-icon" size={RF(2.4)} color={(this.state.toError) ? "#ff7043" : kit.colors.icon.dark} />
                                </TouchableOpacity>
                            </View>}
                        />

                        <View style={{ width: "20%", marginLeft: 10, flexDirection: "row", justifyContent: "space-between", marginRight: 15 }}>
                            <CircularItem type={'small'} style={{}} text={this.renderFloorName(this.state.toFloor)} icon="home-icon" onPress={() => this.openFloorModal(1)} />
                            <CircularItem type={'small'} text={this.liftText(1)} icon="lift-active-icon" color={this.liftColor(1)} onPress={() => this.toggleLift(1)} />
                        </View>
                    </View>

                </Navbar>
                <FloorMenu visible={this.state.openFromFloors} onSelect={(floor) => { this.setState({ openFromFloors: false, fromFloor: floor, callUs: floor === 5 && !this.state.fromLift ? true : false }) }} />
                <FloorMenu visible={this.state.openToFloors} onSelect={(floor) => { this.setState({ openToFloors: false, toFloor: floor, callUs: floor === 5 && !this.state.toLift ? true : false }) }} />
                <VoucherModal visible={this.state.openVoucher} close={() => this.setState({ openVoucher: false, })} onVoucherApplied={(voucher) => this.voucherSubmitted(voucher)} />
            </View>
        );
    }
}

const mapStateToProps = state => {
    return {
        pickup: state.pickupReducer,
        destination: state.destinationReducer,
        voucherid: state.voucherReducer,
        validVendorCode: state.pickupReducer
    }
}

export default connect(mapStateToProps, { setPickup, setDestination, setVoucher, setVendorCode })(Home);