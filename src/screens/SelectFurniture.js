import React from 'react';
import { View, Text, ScrollView, Dimensions, StatusBar, TouchableOpacity, Image, TextInput, TouchableWithoutFeedback } from 'react-native';
import LinearGradient from "react-native-linear-gradient"
import kit from '../assets/styles/kit.js';
import { createIconSetFromIcoMoon } from 'react-native-vector-icons';
import icoMoonConfig from '../assets/icons/selection.json';
import RF from 'react-native-responsive-fontsize';
import styles from '../assets/styles/index.js';
const Icon = createIconSetFromIcoMoon(icoMoonConfig, 'icomoon', 'icomoon.ttf');
const { width, height } = Dimensions.get('window');
import { connect } from 'react-redux';
import CategoryItem from '../components/categoryItem.js';
import Line from "../components/line"
import CircularItem from "../components/circularItem"
import Handle from "../components/handle"
import { request, calculatePrice, renderTime, callSupport, vanRenderIcons } from '../tools/index.js';
import { Navigation } from "react-native-navigation";
import { setItems } from "../actions"
import BottomCard from "../components/bottomCard"
import Submit from "../components/submit"


class SelectFurniture extends React.Component {
    constructor(props) {
        super(props);
        this.fetchCategories()
        this.vanIcons = vanRenderIcons();

        this.state = {
            // code: this.props.route.params.code,
            categories: [{ title: 'Create items', logo: "category-7", color: '#e2d3d7', items: [], count: 0 }],
            furniture: [],
            selectedCategory: { index: 0, title: '', items: [], color: "#e2d3d7" },
            items: [],
            length: '',
            width: '',
            height: '',
            weight: '',
            irrName: '',
            bottomItemsOpen: true,
            callUs: false,
            companyPhone: "",

            price: null,
            time: null,
            totalItems: 0,
            vanType: 0,
            vanLoad: 0,
            van: {
                title: "Caddy Van",
                icon: require("../assets/images/vans/0-0.png")
            },
            increase: false,
            decrease: false,
        }
    }

    componentDidMount() {
        this.calc()
    }

    fetchCategories() {
        request('/app/api/v1/category/list', {}, (err, response) => {
            if (err) {
                console.log("error in fetching list data: ", err)
                return;}

               
            let categories = response.data.categories.concat(this.state.categories);
            categories = categories.map((c, i) => {
                c.items = c.items.map(i => {
                    i.count = 0;
                    return i;
                })
                c.index = i;
                c.count = 0;
                return c
            })

            console.log("Data after fetch list : ", categories)

            this.setState({
                categories: categories,
                furniture: response.data.categories[0].items,
                selectedCategory: categories[0]
            })
        }, "post")

    }

    _renderCategories() {
        return this.state.categories.map((item, index) => {
            return (
                <View key={index} style={{ width: (width / 3), justifyContent: 'center', alignItems: 'center', marginRight: RF(1), marginLeft: 0 }} >
                    <CategoryItem
                        onPress={() => {
                            this.setState({
                                furniture: item.items,
                                selectedCategory: item
                            })
                        }}
                        image={item.logo} title={item.title}
                        count={item.count} color={item.color} />
                </View>
            )
        })
    }

    submitNewItem() {

        let items = this.state.furniture;
        items.push({
            name: this.state.irrName,
            length: this.state.length,
            height: this.state.height,
            weight: this.state.weight,
            width: this.state.width,
            count: 0,
            id: Date.now() * (-1)
        });
        let categories = this.state.categories;
        categories[(categories.length - 1)].items = items;

        this.setState({
            irrName: '',
            length: '',
            width: '',
            height: '',
            weight: '',
            furniture: items,
            categories
        })
    }

    irrSeperator() {
        let items = [];
        this.state.categories.map(c => {
            c.items.map(f => {
                if (!f.id) return
                if (f.id < 0) return
                if (f.count == 0) return
                items.push({ id: f.id, number: f.count })
            })
        })
        let irr_items = this.state.categories[this.state.categories.length - 1].items.map(irr => {

            return {
                length: Number(irr.length),
                width: Number(irr.width),
                height: Number(irr.height),
                weight: Number(irr.weight),
                number: irr.count,
            }
        })

        this.state.items = items;
        this.state.irr_items = irr_items;
        let companyPhone = this.state.companyPhone

        return { items, irr_items, companyPhone }
    }
    fetchingId = null
    calc() {

        let fetchingId = Date.now();
        this.fetchingId = fetchingId;
        let { items, irr_items } = this.irrSeperator()

        calculatePrice(this.props.pickup, this.props.destination, this.props.voucherid, items, irr_items, "furniture", {}, (err, result) => {
            if (fetchingId != this.fetchingId) return
            if (err) return;

            if (result.data.min_price === -1) {
                this.setState({
                    price: NaN,
                    time: NaN,
                    callUs: true,
                    vanLoad: 100,
                    vanType: 4,
                    companyPhone: result.data.companyPhone
                });
                this.renderVan()
                return;
            }

            this.setState({
                price: result.data.min_price,
                time: result.data.time,
                callUs: false,
                vanLoad: result.data.load,
                vanType: result.data.vehicle_type,
                companyPhone: result.data.companyPhone
            })
            this.renderVan()
        })
    }
    
    renderVan() {
        let van = {}
        var vanSelected = this.state.vanType;
        let load = Math.floor(this.state.vanLoad / 2.5);
        let loads = "";

        switch (vanSelected) {
            case 0:
                load = Math.floor(this.state.vanLoad / 4);
                van.title = "Caddy Van";
                van.icon = this.vanIcons[0][load];
                break;
            case 1:
                van = {
                    title: "Transit Van",
                    icon: this.vanIcons[1][load]
                }
                break;
            case 2:
                van = {
                    title: "Sprinter van",
                    icon: this.vanIcons[2][load]
                }
                break;
            case 3:
                van = {
                    title: "Luton Van",
                    icon: this.vanIcons[3][load]
                }
                break;
            default:
                van = {
                    title: "Too many items",
                    icon: require("../assets/images/vans/4-0.png")
                }
        }
        this.setState({ van })
    }
    newFurniture() {
        return (
            <View style={styles.newFurniture} >

                <View style={{ width: '80%', height: '100%', borderRightWidth: 0.5, borderRightColor: '#f5f5f5' }} >

                    <View style={{ width: '100%', height: '34%', flexDirection: 'row', borderBottomWidth: 0.5, borderBottomColor: '#f5f5f5' }} >
                        <TouchableWithoutFeedback underlayColor="transparent" style={{ width: '100%', height: '100%' }} onPress={() => { this.refs.irrName.focus() }}>
                            <View style={styles.gridRow} >
                                <Text>Name</Text>
                                <LinearGradient
                                    start={{ x: 0, y: 0 }}
                                    end={{ x: 1, y: 0 }}
                                    style={styles.newInputHolderName}
                                    colors={[kit.colors.optionsBackground[1], kit.colors.optionsBackground[0]]}>
                                    <Text style={{ textAlign: 'center' }}>{this.state.irrName}</Text>
                                    <TextInput
                                        value={this.state.irrName} ref="irrName"
                                        onChangeText={(value) => {
                                            //value = value.length > 3 ? value.substr(0, 3) : value;
                                            this.setState({ irrName: value })
                                        }}
                                        style={{ width: 0, height: 0, position: 'absolute', display: "none" }}
                                        underlineColorAndroid="transparent" disableFullscreenUI />
                                </LinearGradient>
                            </View>
                        </TouchableWithoutFeedback>
                    </View>

                    <View style={{ width: '100%', height: '33%', flexDirection: 'row', borderBottomWidth: 0.5, borderBottomColor: '#f5f5f5' }} >
                        <TouchableWithoutFeedback underlayColor="transparent" style={{ width: '50%', height: '100%' }} onPress={() => { this.refs.width.focus() }}>
                            <View style={styles.newGridBordered} >
                                <Text>Width</Text>
                                <LinearGradient
                                    start={{ x: 0, y: 0 }}
                                    end={{ x: 1, y: 0 }}
                                    style={styles.newInputHolder}
                                    colors={[kit.colors.optionsBackground[1], kit.colors.optionsBackground[0]]}>
                                    <Text style={{ textAlign: 'center' }}>{this.state.width} cm</Text>
                                    <TextInput
                                        value={this.state.width} ref="width" keyboardType="numeric"
                                        onChangeText={(value) => {
                                            value = value.length > 3 ? value.substr(0, 3) : value;
                                            this.setState({ width: value })
                                        }}
                                        style={{ width: 0, height: 0, position: 'absolute', display: "none" }}
                                        underlineColorAndroid="transparent" disableFullscreenUI />
                                </LinearGradient>
                            </View>
                        </TouchableWithoutFeedback>
                        <TouchableWithoutFeedback underlayColor="transparent" style={{ width: '50%', height: '100%' }} onPress={() => { this.refs.height.focus() }}>
                            <View style={styles.newGrid} >
                                <Text>Height</Text>
                                <LinearGradient
                                    start={{ x: 0, y: 0 }}
                                    end={{ x: 1, y: 0 }}
                                    style={styles.newInputHolder}
                                    colors={[kit.colors.optionsBackground[1], kit.colors.optionsBackground[0]]}>
                                    <Text style={{ textAlign: 'center' }}>{this.state.height} cm</Text>
                                    <TextInput
                                        value={this.state.height} ref="height" keyboardType="numeric"
                                        onChangeText={(value) => {
                                            value = value.length > 3 ? value.substr(0, 3) : value;
                                            this.setState({ height: value })
                                        }}
                                        style={{ width: 0, height: 0, position: 'absolute', display: "none" }}
                                        underlineColorAndroid="transparent" disableFullscreenUI />
                                </LinearGradient>
                            </View>
                        </TouchableWithoutFeedback>
                    </View>

                    <View style={{ width: '100%', height: '33%', flexDirection: 'row' }} >
                        <TouchableWithoutFeedback underlayColor="transparent" style={{ width: '50%', height: '100%' }} onPress={() => { this.refs.length.focus() }}>
                            <View style={styles.newGridBordered} >
                                <Text>Length</Text>
                                <LinearGradient
                                    start={{ x: 0, y: 0 }}
                                    end={{ x: 1, y: 0 }}
                                    style={styles.newInputHolder}
                                    colors={[kit.colors.optionsBackground[1], kit.colors.optionsBackground[0]]}>
                                    <Text style={{ textAlign: 'center' }}>{this.state.length} cm</Text>
                                    <TextInput
                                        value={this.state.length} ref="length" keyboardType="numeric"
                                        onChangeText={(value) => {
                                            value = value.length > 3 ? value.substr(0, 3) : value;
                                            this.setState({ length: value })
                                        }}
                                        style={{ width: 0, height: 0, position: 'absolute', display: "none" }}
                                        underlineColorAndroid="transparent" disableFullscreenUI />
                                </LinearGradient>
                            </View>
                        </TouchableWithoutFeedback>
                        <TouchableWithoutFeedback underlayColor="transparent" style={{ width: '50%', height: '100%' }} onPress={() => { this.refs.weight.focus() }}>
                            <View style={styles.newGrid} >
                                <Text>Weight</Text>
                                <LinearGradient
                                    start={{ x: 0, y: 0 }}
                                    end={{ x: 1, y: 0 }}
                                    style={styles.newInputHolder}
                                    colors={[kit.colors.optionsBackground[1], kit.colors.optionsBackground[0]]}>
                                    <Text style={{ textAlign: 'center' }}>{this.state.weight} kg</Text>
                                    <TextInput
                                        value={this.state.weight} ref="weight" keyboardType="numeric"
                                        onChangeText={(value) => {
                                            value = value.length > 3 ? value.substr(0, 3) : value;
                                            this.setState({ weight: value })
                                        }}
                                        style={{ width: 0, height: 0, position: 'absolute', display: "none" }}
                                        underlineColorAndroid="transparent" disableFullscreenUI />
                                </LinearGradient>
                            </View>
                        </TouchableWithoutFeedback>
                    </View>
                </View>

                <View style={{ width: '20%', height: '100%', justifyContent: 'center', alignItems: 'center' }}>

                    <TouchableOpacity onPress={() => { (this.state.weight && this.state.height && this.state.length && this.state.width && this.state.irrName) ? this.submitNewItem() : undefined }} style={{ borderRadius: 15 }} underlayColor={'rgba(255,255,255,0.5)'}>
                        <LinearGradient
                            start={{ x: 0, y: 0 }}
                            end={{ x: 1, y: 0 }}
                            style={{ width: RF(6.2), height: RF(6.2), borderRadius: 15, justifyContent: 'center', alignItems: 'center' }}
                            colors={(this.state.weight && this.state.height && this.state.length && this.state.width && this.state.irrName) ? kit.colors.callButton : kit.colors.disabledSubmitButton}>
                            <Icon name="tick-icon" color="#fff" size={RF(4.5)} />
                        </LinearGradient>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }

    addFurniture(id, delta, auto) {
        if (!delta) delta = 1
        let { selectedCategory, categories, furniture, totalItems } = this.state
        selectedCategory.count += delta;
        var fi = furniture.findIndex(i => String(i.id) == String(id))
        if (!furniture[fi].count) furniture[fi].count = 0;
        furniture[fi].count += delta;
        this.setState({
            categories,
            selectedCategory,
            furniture,
            totalItems: totalItems + delta
        })
        if (!auto)
            this.calc()
    }
    removeFurniture(id, delta, auto) {
        if (!delta) delta = 1

        let { selectedCategory, categories, furniture, totalItems } = this.state

        var fi = furniture.findIndex(i => String(i.id) == String(id))
        if (!furniture[fi].count || furniture[fi] == 0) return;
        if (furniture[fi].count - delta < 0) delta = furniture[fi].count
        selectedCategory.count -= delta;
        furniture[fi].count -= delta;

        this.setState({
            categories,
            selectedCategory,
            furniture,
            totalItems: totalItems - delta
        })
        if (!auto)
            this.calc()
    }
    increase(id, index, delta) {
        if (index % 10 == 0) delta++;
        this.addFurniture(id, delta)
        index++;
        let timer = 50 / (index * 0.5)
        setTimeout(() => {
            if (this.state.increase == false) return
            this.increase(id, index, delta, true)
        }, timer)
    }
    decrease(id, index, delta) {
        if (index % 10 == 0) delta++;
        this.removeFurniture(id, delta)
        index++;
        let timer = 50 / (index * 0.5)
        setTimeout(() => {
            if (this.state.decrease == false) return
            this.decrease(id, index, delta, true)
        }, timer)
    }
    renderItems() {
        return this.state.furniture.map((item, index) => {
            return (
                <View key={index} style={styles.furnitureItem}>
                    <Text>{item.name}</Text>
                    <LinearGradient
                        start={{ x: 0, y: 0 }}
                        end={{ x: 1, y: 0 }}
                        style={styles.furnitureCounter}
                        colors={kit.colors.optionsBackground}>
                        <TouchableOpacity onPressOut={() => this.setState({ decrease: false }, () => this.calc())} onLongPress={() => { this.setState({ decrease: true }, () => this.decrease(item.id, 0, 0)) }} onPress={() => this.removeFurniture(item.id)} style={{ minHeight: 40, minWidth: 40, justifyContent: 'center', alignItems: 'center' }} >
                            <Text style={{ color: 'red', fontSize: 20 }} >-</Text>
                        </TouchableOpacity>
                        <Text>{item.count}</Text>
                        <TouchableOpacity onPressOut={() => this.setState({ increase: false }, () => this.calc())} onLongPress={() => { this.setState({ increase: true }, () => this.increase(item.id, 0, 0)) }} onPress={() => this.addFurniture(item.id)} style={{ minHeight: 40, minWidth: 40, paddingHorizontal: 10, justifyContent: 'center', alignItems: 'center' }} >
                            <Text style={{ color: 'green' }} >+</Text>
                        </TouchableOpacity>
                    </LinearGradient>
                    <View style={[styles.furnitureLabel, { backgroundColor: this.state.selectedCategory.color }]} />
                </View>
            )
        })
    }

    renderIrregularItems() {
        return this.state.furniture.map((item, index) => {
            return (
                <View key={index} style={styles.furnitureItem}>
                    <Text>{`${item.name} ${item.width} x ${item.length} x ${item.height} cm. ${item.weight} kg`}</Text>
                    <LinearGradient
                        start={{ x: 0, y: 0 }}
                        end={{ x: 1, y: 0 }}
                        style={styles.furnitureCounter}
                        colors={[kit.colors.optionsBackground[1], kit.colors.optionsBackground[0]]}>
                        <TouchableOpacity onPressOut={() => this.setState({ decrease: false })} onLongPress={() => { this.setState({ decrease: true }, () => this.decrease(item.id, 0, 0)) }} onPress={() => this.removeFurniture(item.id)} style={{ minHeight: 40, minWidth: 40, justifyContent: 'center', alignItems: 'center' }} >
                            <Text style={{ color: 'red', fontSize: 20 }} >-</Text>
                        </TouchableOpacity>
                        <Text>{item.count}</Text>
                        <TouchableOpacity onPressOut={() => this.setState({ increase: false })} onLongPress={() => { this.setState({ increase: true }, () => this.increase(item.id, 0, 0)) }} onPress={() => this.addFurniture(item.id)} style={{ minHeight: 40, minWidth: 40, paddingHorizontal: 10, justifyContent: 'center', alignItems: 'center' }} >
                            <Text style={{ color: 'green' }} >+</Text>
                        </TouchableOpacity>
                    </LinearGradient>
                    <View style={[styles.furnitureLabel, { backgroundColor: "#9ef1fc" }]} />
                </View>
            )
        })
    }

    isAdminVoucher() {
        if (String(this.props.voucherid).indexOf("9999") == 0) return true
        return false
    }

    submitItems() {
        this.props.setItems(this.irrSeperator())
        Navigation.push(this.props.componentId, { component: { name: "ShipmentOptions", id: "ShipmentOptions" } })
    }

    render() {
        return (
            <View style={{ flex: 1, width: '100%', backgroundColor: '#f5f5f5' }} >
                <LinearGradient
                    start={{ x: 0, y: 0 }}
                    end={{ x: 1, y: 0 }}
                    colors={kit.colors.navigation}
                    style={{ width: "100%", paddingVertical: 20, top: 0, left: 0, zIndex: 999 }}>
                    <View style={{ justifyContent: "space-between", flexDirection: "row", paddingHorizontal: 20 }}>
                        <Icon onPress={() => { Navigation.pop(this.props.componentId) }} name="back-arrow-icon" size={25} color="white" />
                        <Text style={{ color: "white" }}>Man and Van Supermarket</Text>
                        <Icon onPress={() => callSupport(this.state.companyPhone)} name="call-icon-mini" size={25} color="white" />
                        <StatusBar hidden backgroundColor="transparent" />
                    </View>
                    <View style={{ paddingTop: 20, }}>
                        <ScrollView showsHorizontalScrollIndicator={false} contentContainerStyle={{ flexGrow: 1 }} horizontal >
                            {this._renderCategories()}
                        </ScrollView>
                    </View>
                </LinearGradient>

                <ScrollView contentContainerStyle={{ flexGrow: 1, width: '100%', position: 'relative', justifyContent: 'space-between' }} >
                    <View style={{ width: '100%', minHeight: height * 2.5, flexDirection: 'column', alignItems: 'center' }} >
                        {this.state.selectedCategory.title === "Create items" &&
                            this.newFurniture()
                        }
                        {this.state.selectedCategory.title !== "Create items" ?
                            this.renderItems() :
                            this.renderIrregularItems()
                        }
                    </View>
                </ScrollView>

                <View style={{ width: "100%", justifyContent: "center", alignItems: "center", position: "absolute", bottom: 0, left: 0 }}>
                    {/* bottom card */}
                    <BottomCard body={
                        <View>
                            <View style={{ flexDirection: 'row', alignItems: 'center', height: RF(6) }} >
                                <Image style={{ width: RF(16), height: RF(16), position: 'absolute', left: RF(4) }} resizeMode="contain" source={this.state.van.icon} />
                                <Text style={{ right: RF(5), position: 'absolute' }}>{this.state.van.title}</Text>
                            </View>
                            <Line />
                            <View style={{ flexDirection: "row", justifyContent: "space-between", paddingTop: 20, paddingHorizontal: 20 }}>
                                <CircularItem text={this.state.totalItems + " items"} icon="address-icon" />
                                <CircularItem text={renderTime(this.state.time)} icon="home-icon" />
                                <CircularItem text={(this.state.price) ? "From £" + this.state.price : "Min Price"} icon="price-icon" />
                            </View>
                            {/* <Line /> */}
                        </View>
                    }
                        action={
                            <Submit
                                icon={this.state.callUs ? "call-icon-mini" : "next-arrow-icon"}
                                onPress={() => {
                                    if (this.state.callUs) {
                                        callSupport(this.state.companyPhone)
                                    } else {
                                        this.submitItems()
                                    }
                                }}
                                text={this.state.callUs ? "Call us" : "Next"}
                                enabled={this.state.totalItems > 0} />
                        } />

                </View>
            </View>
        )
    }
}

const mapStateToProps = state => {
    return {
        pickup: state.pickupReducer.pickup,
        destination: state.destinationReducer.destination,
        voucherid: state.voucherReducer.voucherid,
    }
}

export default connect(mapStateToProps, { setItems })(SelectFurniture);