import React from "react";
import {
  View,
  Text,
  ScrollView,
  TouchableOpacity,
  Dimensions,
  StatusBar,
} from "react-native";
import { createIconSetFromIcoMoon } from "react-native-vector-icons";
import icoMoonConfig from "../assets/icons/selection.json";
import RF from "react-native-responsive-fontsize";
import Schedule from "../components/schedule.js";
import styles from "../assets/styles/index.js";
const Icon = createIconSetFromIcoMoon(icoMoonConfig, "icomoon", "icomoon.ttf");
const { width, height } = Dimensions.get("window");
import { connect } from "react-redux";
import { calculatePrice, request, callSupport } from "../tools/index.js";
import { Navigation } from "react-native-navigation";
import BottomCard from "../components/bottomCard";
import Submit from "../components/submit";
import { setSchedule } from "../actions";
import Navbar from "../components/navbar.js";

class CompaniesSchedule extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      validVendorCode: this.props.validVendorCode,
      companies: [],
      selectedCompany: "",
      selectedDate: "",
      selectedCompanyInfo: null,
    };
  }

  anyDateSelected() {
    if (!this.state.selectedCompany && !this.state.selectedDate) {
      return false;
    }
    return true;
  }

  componentDidMount() {
    calculatePrice(
      this.props.pickup.pickup,
      this.props.destination.destination,
      this.props.voucherid.voucherid,
      this.props.items,
      this.props.irr_items,
      "calendar",
      {
        help: this.props.options.help,
        passenger: this.props.options.passenger,
        preferred_time: this.props.options.preferred_time,
        days: 30,
        extra_stop: this.props.options.extra_stop,
      },
      (err, response) => {
        if (response.code === 200) {
          let data = response.data.full_result;
          const arrayOfCompanies = data;
          const num = this.state.validVendorCode;
          const vendor = arrayOfCompanies.filter(
            (item) => item.supplier_id === num
          );
          // let selectedItem =
          this.setState({ companies: vendor });
        }
      }
    );
  }

  renderSchedules() {
    return this.state.companies.map((item, index) => {
      return (
        <Schedule
          onChange={(date, company) => {
            this.setState({
              selectedCompany: company,
              selectedDate: date,
              selectedCompanyInfo: item,
            });
          }}
          drivers={item.drivers}
          van={item.vehicle_type}
          logo={item.logo}
          selectedCompany={this.state.selectedCompany}
          selectedDate={this.state.selectedDate}
          name={item.name}
          postCode={item.post_code}
          key={index}
          date={item.dates}
        />
      );
    });
  }

  render() {
    return (
      <View style={{ flex: 1, width: "100%", paddingTop: 80 }}>
        <ScrollView
          contentContainerStyle={{
            flexGrow: 1,
            alignItems: "center",
            width: "100%",
            paddingBottom: height * 0.5,
          }}
        >
          {/* <Schedule /> */}
          {this.renderSchedules()}
        </ScrollView>
        <BottomCard
          action={
            <Submit
              icon="next-arrow-icon"
              onPress={() => {
                this.props.setSchedule({
                  company: this.state.selectedCompanyInfo,
                  date: this.state.selectedDate,
                });
                Navigation.push(this.props.componentId, {
                  component: { name: "ShipmentSummary", id: "ShipmentSummary" },
                });
              }}
              text={"Next"}
              enabled={this.anyDateSelected()}
            />
          }
        />
        <Navbar
          leftItem={
            <Icon
              onPress={() => Navigation.pop(this.props.componentId)}
              name="back-arrow-icon"
              size={25}
              color="white"
            />
          }
          rightItem={
            <Icon
              onPress={() => callSupport(this.state.companies[0].phone)}
              name="call-icon-mini"
              size={25}
              color="white"
            />
          }
        />
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    pickup: state.pickupReducer,
    validVendorCode: state.pickupReducer.validVendorCode,
    destination: state.destinationReducer,
    voucherid: state.voucherReducer,
    items: state.itemsReducer.items,
    irr_items: state.itemsReducer.irr_items,
    options: state.optionsReducer,
  };
};

export default connect(mapStateToProps, { setSchedule })(CompaniesSchedule);
