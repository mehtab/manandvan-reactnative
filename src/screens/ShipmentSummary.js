import React from 'react';
import { View, Text, Dimensions, TextInput, TouchableOpacity, TouchableHighlight, ScrollView, Linking, KeyboardAvoidingView, Platform, ActivityIndicator } from 'react-native';
import { connect } from 'react-redux';
import Navbar from '../components/navbar';
import styles from '../assets/styles';
import LinearGradient from "react-native-linear-gradient"
import kit from '../assets/styles/kit.js';
import { createIconSetFromIcoMoon } from 'react-native-vector-icons';
import icoMoonConfig from '../assets/icons/selection.json';
const { width, height } = Dimensions.get('window');
import RF from 'react-native-responsive-fontsize';
import { Navigation } from 'react-native-navigation';
import { callSupport, getOneTimeToken, sendBookingRequest } from "../tools"
import BottomCard from "../components/bottomCard"
import Submit from '../components/submit.js';
import { requestOneTimePayment } from 'react-native-paypal';
import ConfirmModal from '../components/confirmModal';
import ErrorModal from '../components/errorModal';

const Icon = createIconSetFromIcoMoon(icoMoonConfig, 'icomoon', 'icomoon.ttf');
class ShipmentSummary extends React.Component {
    constructor(props) {
        super(props);

        var p = 0;
        var s = this.props.schedule.date;

        for (var i = 0; i < this.props.schedule.company.dates.length; i++) {
            var d = new Date(this.props.schedule.company.dates[i].date);
            var dstr = d.toISOString().split('T')[0];

            if (dstr === s) {
                p = this.props.schedule.company.dates[i].price;
                break;
            }
        }

        this.state = {

            pickup_zip_code: this.props.pickup.postalCode,
            pickup_from: this.props.pickup.fullAddress,
            pickup_house_name: '',

            destination_zip_code: this.props.destination.postalCode,
            destination_from: this.props.destination.fullAddress,
            destination_house_name: '',
            extra_stop_address: this.props.options.extra_stop_address,
            extra_stop_house_name: '',
            company: this.props.schedule.company,
            date: this.props.schedule.date,
            items: this.props.items,
            irr_items: this.props.irr_items,
            service_selected: '',
            part_payment: 0,
            total_price: p,
            number_of_item_confirm: false,
            optin_confirm: false,
            terms_confirm: false,
            deposite_paied: false,
            openConfirm: false,
            openError: false,
            spinner: false,
            isPayPalActive: false,

            notes: '',
            email: '',
            phone: '',
            inputHeight: 40
        }
    }
    services() {
        var values = this.props.options.keys().map(k => this.props.options[k])
        var service_selected = values.join(",")
        this.setState({
            service_selected
        })
    }

    getDepositeButtonName() {
        if (this.state.isPayPalActive) {
            return "Wait ...";
        }
        // return "Pay deposit £" + this.getDepositeAmount();
        return "SUBMIT"

    }

    getDepositeAmount() {
        var commision = this.state.company.commision;
        return Math.ceil(this.state.total_price * commision / 100);
    }

    emailValid() {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(this.state.email).toLowerCase());
    }

    phoneValid() {
        return this.state.phone.length > 5;
    }

    payDepoiteEnable() {
        if (this.state.isPayPalActive) {
            return false;
        }

        if (this.isAdminVoucher()) {
            //return this.state.number_of_item_confirm && this.state.optin_confirm && this.state.terms_confirm && this.emailValid() && this.phoneValid();
            return this.state.number_of_item_confirm && this.state.terms_confirm && this.emailValid() && this.phoneValid();

        }
        // return this.state.number_of_item_confirm && this.state.terms_confirm;
        return this.state.number_of_item_confirm;

    }


    async paypalPaymentInit() {

        // var token = await getOneTimeToken();
        // var amount = this.getDepositeAmount() + '';
        // 1- Disable button
        this.setState({ isPayPalActive: true });
        console.log("0 step")


        // 2- 
        try {
            // const {
            // nonce,
            // payerId,
            // email,
            // firstName,
            // lastName,
            // phone 
            // } = 

            const nonce = ''
            const payerId = ''
            const email = ''
            const firstName = ''
            const lastName = ''
            const phone = ''


            // Code for paypal payment
            // = await requestOneTimePayment(
            // token,
            // {
            //     amount: amount, // required
            //     // any PayPal supported currency (see here: https://developer.paypal.com/docs/integration/direct/rest/currency-codes/#paypal-account-payments)
            //     currency: 'GBP',
            //     // any PayPal supported locale (see here: https://braintree.github.io/braintree_ios/Classes/BTPayPalRequest.html#/c:objc(cs)BTPayPalRequest(py)localeCode)
            //     localeCode: 'en_GB', 
            //     shippingAddressRequired: false,
            //     userAction: 'commit', // display 'Pay Now' on the PayPal review page
            //     // one of 'authorize', 'sale', 'order'. defaults to 'authorize'. see details here: https://developer.paypal.com/docs/api/payments/v1/#payment-create-request-body
            //     intent: 'authorize', 
            // }
            // );



            // Todo:
            // 1- Open spinner Modal
            this.setState({ spinner: true });

            // 2- Send object to server
            var bookObject = {
                client_email: this.state.email,
                client_phone: this.state.phone,
                client_note: this.state.notes,
                transfer_date: this.state.date,
                booking_json: {
                    pickup: this.state.pickup_from,
                    pickup_house_name: this.state.pickup_house_name,
                    destination: this.state.destination_from,
                    destination_house_name: this.state.destination_house_name,
                    extra_stop: this.state.extra_stop_address,
                    extra_stop_house_name: this.state.extra_stop_house_name,
                    original_addresses: {
                        pickup: this.props.pickup,
                        destination: this.props.destination
                    },
                    options: this.props.options,
                    company: this.props.schedule.company,
                    date: this.props.schedule.date,
                    items: this.props.items,
                    irr_items: this.props.irr_items,
                    total_price: this.state.total_price
                },
                supplier_id: this.props.schedule.company.supplier_id,
                voucher_id: this.props.voucherid,
                transaction_obj: {
                    nonce: nonce,
                    payerId: payerId,
                    email: email,
                    firstName: firstName,
                    lastName: lastName,
                    phone, phone,
                    payment: this.getDepositeAmount(),
                    is_confirmed: true
                }
            };
            bookObject.booking_json.company.dates = "";
            console.log("1 step")
            var paid = await sendBookingRequest(bookObject);
            console.log("2 step")

            this.setState({ spinner: false });
            if (paid) {
                this.setState({ openConfirm: true });
            } else {
                this.setState({ openError: true });
            console.log("3 step")

            }
        } catch (e) {
            console.log("4 step")

            this.setState({ isPayPalActive: false });
        }

    }

    renderFloorLift(loc) {
        var res = "";

        switch (loc.floor) {
            case -1:
                res = "Basement floor"
                break;
            case 0:
                res = "Ground floor"
                break;
            case 1:
                res = "First floor"
                break;
            case 2:
                res = "Second floor"
                break;
            case 3:
                res = "Third floor"
                break;
            case 4:
                res = "Forth floor"
                break;
            case 5:
                res = "Fiveth floor or upper"
                break;
        }
        res += (loc.lift ? " with lift" : " without lift")
        return res;
    }

    changeConfirm(w) {
        var c = false;
        switch (w) {
            case "items":
                c = this.state.number_of_item_confirm;
                this.setState({ number_of_item_confirm: !c });
                break;
            case "optin":
                c = this.state.optin_confirm;
                this.setState({ optin_confirm: !c });
                break;
            case "terms":
                c = this.state.terms_confirm;
                this.setState({ terms_confirm: !c });
                break;
        }
    }

    sameDay(d1, d2) {
        return d1.getFullYear() === d2.getFullYear() && d1.getMonth() === d2.getMonth() && d1.getDate() === d2.getDate();
    }

    componentDidMount() {
        // alert(this.props.validVendorCode)
    }

    renderDate() {
        return (new Date(this.state.date)).toDateString()
    }

    renderTime(t) {
        return (Math.ceil(t * 2) / 2).toFixed(1) + " h";
    }

    renderDistance() {
        return this.state.company.distance.m_plus.toFixed(1) + " mi";
    }

    renderNumberOfItems() {
        var count = 0;
        for (var i = 0; i < this.state.items.length; i++) {
            count += this.state.items[i].number;
        }

        for (var i = 0; i < this.state.irr_items.length; i++) {
            count += this.state.irr_items[i].number;
        }

        return count;
    }

    renderVan() {
        var type = this.state.company.vehicle_type;
        switch (type) {
            case 0:
                return "Caddy van"
            case 1:
                return "Transit van"
            case 2:
                return "Sprinter van"
            case 3:
                return "Luton van"
            default:
                return "Caddy van"
        }
    }

    renderHelp() {
        switch (this.props.options.help) {
            case 0: return "No"
            case 1: return "Yes"
            case 2: return "2 Helpers"
            default: return "No"
        }
    }
    renderPassenger() {
        switch (this.props.options.passenger) {
            case 0: return "No"
            case 1: return "One"
            case 2: return "Two"
            default: return "No"
        }
    }
    renderPreferredTime() {
        switch (this.props.options.preferred_time) {
            case 0: return "Anytime"
            case 1: return "Before 8 AM"
            case 2: return "8 AM - 12 PM"
            case 3: return "1 PM - 6 PM"
            case 4: return "After 6 PM"
            default: return "Anytime"
        }
    }
    openWebsite(website) {
        if (website.indexOf("http://") < 0 && website.indexOf("https://") < 0) {
            website = "http://" + website;
        }
        Linking.openURL(website);
    }

    isAdminVoucher() {
        if (String(this.props.voucherid).indexOf("9999") == 0) return true
        return false
    }
    updateSize = (h) => {
        this.setState({
            inputHeight: h
        });
    }
    render() {
        return (
            <View style={{ flex: 1, width: '100%', backgroundColor: '#f5f5f5', paddingTop: 70 }} >
                <Navbar
                    leftItem={<Icon onPress={() => Navigation.pop(this.props.componentId)} name="back-arrow-icon" size={25} color="white" />}
                    rightItem={<Icon onPress={() => callSupport(this.state.company.phone)} name="call-icon-mini" size={25} color="white" />} />

                <ScrollView contentContainerStyle={{ flexGrow: 1, width: '100%', alignItems: 'center', position: 'relative', justifyContent: 'space-between', paddingBottom: height * 0.2 }} >
                    <View style={styles.helpCard} >
                        <View style={styles.summaryItem} >
                            <Text>House number / name</Text>
                            <TextInput
                                value={this.state.pickup_house_name}
                                editable={true}
                                onChangeText={(value) => {
                                    this.setState({ pickup_house_name: value })
                                }}
                                style={[styles.input, { padding: 15 }, styles.summaryItemValueEditable]}
                                underlineColorAndroid="transparent" disableFullscreenUI />
                        </View>
                        <View style={styles.summaryItem} >
                            <Text>Pickup address</Text>
                            <LinearGradient
                                start={{ x: 0, y: 0 }}
                                end={{ x: 1, y: 0 }}
                                style={styles.summaryItemValue}
                                colors={[kit.colors.optionsBackground[1], kit.colors.optionsBackground[0]]}>
                                <Text style={styles.summaryItemText}>{this.state.pickup_from}</Text>
                                <Text style={styles.summaryItemText}>{this.renderFloorLift(this.props.pickup)}</Text>
                            </LinearGradient>
                        </View>
                        {!!this.props.options.extra_stop && <View style={styles.summaryItem} >
                            <Text>House number / name</Text>
                            <TextInput
                                value={this.state.extra_stop_house_name}
                                editable={true}
                                onChangeText={(value) => {
                                    this.setState({ extra_stop_house_name: value })
                                }}
                                style={[styles.input, { padding: 15 }, styles.summaryItemValueEditable]}
                                underlineColorAndroid="transparent" disableFullscreenUI />
                        </View>}
                        {!!this.props.options.extra_stop && <View style={styles.summaryItem} >
                            <Text>Via (extra stop)</Text>
                            <TextInput
                                value={this.state.extra_stop_address}
                                editable={true}
                                onChangeText={(value) => {
                                    this.setState({ extra_stop_address: value })
                                }}
                                style={[styles.input, { padding: 15 }, styles.summaryItemValueEditable]}
                                underlineColorAndroid="transparent" disableFullscreenUI />
                        </View>
                        }

                        <View style={styles.summaryItem} >
                            <Text>House number / name</Text>
                            <TextInput
                                value={this.state.destination_house_name}
                                editable={true}
                                onChangeText={(value) => {
                                    this.setState({ destination_house_name: value })
                                }}
                                style={[styles.input, { padding: 15 }, styles.summaryItemValueEditable]}
                                underlineColorAndroid="transparent" disableFullscreenUI />
                        </View>
                        <View style={styles.summaryItem} >
                            <Text style={{ alignItems: 'flex-start' }}>Destination address</Text>
                            <LinearGradient
                                start={{ x: 0, y: 0 }}
                                end={{ x: 1, y: 0 }}
                                style={styles.summaryItemValue}
                                colors={[kit.colors.optionsBackground[1], kit.colors.optionsBackground[0]]}>
                                <Text style={styles.summaryItemText}>{this.state.destination_from}</Text>
                                <Text style={styles.summaryItemText}>{this.renderFloorLift(this.props.destination)}</Text>
                            </LinearGradient>
                        </View>


                        <View style={styles.summaryItem} >
                            <Text>Distance</Text>
                            <LinearGradient
                                start={{ x: 0, y: 0 }}
                                end={{ x: 1, y: 0 }}
                                style={styles.summaryItemValue}
                                colors={[kit.colors.optionsBackground[1], kit.colors.optionsBackground[0]]}>
                                <Text>{this.renderDistance()}</Text>
                            </LinearGradient>
                        </View>
                        <View style={styles.summaryItem} >
                            <Text>Drive time</Text>
                            <LinearGradient
                                start={{ x: 0, y: 0 }}
                                end={{ x: 1, y: 0 }}
                                style={styles.summaryItemValue}
                                colors={[kit.colors.optionsBackground[1], kit.colors.optionsBackground[0]]}>
                                <Text>{this.renderTime(this.state.company.distance.round_time)} </Text>
                            </LinearGradient>
                        </View>
                    </View>
                    <View style={styles.helpCard}>
                        <View style={styles.summaryItem} >
                            <Text>Van type</Text>
                            <LinearGradient
                                start={{ x: 0, y: 0 }}
                                end={{ x: 1, y: 0 }}
                                style={styles.summaryItemValue}
                                colors={[kit.colors.optionsBackground[1], kit.colors.optionsBackground[0]]}>
                                <Text>{this.renderVan()}</Text>
                            </LinearGradient>
                        </View>
                        <View style={[styles.summaryItem, { borderBottomColor: 'transparent' }]} >
                            <Text>Help required</Text>
                            <LinearGradient
                                start={{ x: 0, y: 0 }}
                                end={{ x: 1, y: 0 }}
                                style={styles.summaryItemValue}
                                colors={[kit.colors.optionsBackground[1], kit.colors.optionsBackground[0]]}>
                                <Text>{this.renderHelp()}</Text>
                            </LinearGradient>
                        </View>
                        <View style={[styles.summaryItem, { borderBottomColor: 'transparent' }]} >
                            <Text>Passenger</Text>
                            <LinearGradient
                                start={{ x: 0, y: 0 }}
                                end={{ x: 1, y: 0 }}
                                style={styles.summaryItemValue}
                                colors={[kit.colors.optionsBackground[1], kit.colors.optionsBackground[0]]}>
                                <Text>{this.renderPassenger()}</Text>
                            </LinearGradient>
                        </View>
                        <View style={[styles.summaryItem, { borderBottomColor: 'transparent' }]} >
                            <Text>Selected date</Text>
                            <LinearGradient
                                start={{ x: 0, y: 0 }}
                                end={{ x: 1, y: 0 }}
                                style={styles.summaryItemValue}
                                colors={[kit.colors.optionsBackground[1], kit.colors.optionsBackground[0]]}>
                                <Text>{this.renderDate()}</Text>
                            </LinearGradient>
                        </View>
                        <View style={[styles.summaryItem, { borderBottomColor: 'transparent' }]} >
                            <Text>Preferred time</Text>
                            <LinearGradient
                                start={{ x: 0, y: 0 }}
                                end={{ x: 1, y: 0 }}
                                style={styles.summaryItemValue}
                                colors={[kit.colors.optionsBackground[1], kit.colors.optionsBackground[0]]}>
                                <Text>{this.renderPreferredTime()}</Text>
                            </LinearGradient>
                        </View>
                    </View>

                    <View style={styles.helpCard} >
                        {this.isAdminVoucher() && <View style={styles.summaryInputView}>
                            <TextInput
                                maxLength={48}
                                keyboardType='email-address'
                                editable={!this.state.loading}
                                value={this.state.email}
                                onChangeText={(value) => this.setState({ email: value })}
                                style={[styles.input, styles.summaryInput, !this.payDepoiteEnable() && !this.emailValid() ? styles.summaryInputWrong : {}]} placeholder="Email (mandatory)" />
                            <Icon name={"email-icon"} color={kit.colors.icon.dark} size={RF(4)} style={{ position: "absolute", paddingRight: 15 }} />
                        </View>}

                        {this.isAdminVoucher() && <View style={{ marginHorizontal: 10, flexDirection: "row", justifyContent: "flex-end", alignItems: "center" }}>
                            <TextInput
                                keyboardType='numeric'
                                maxLength={14}
                                editable={!this.state.loading}
                                value={this.state.phone}
                                onChangeText={(value) => this.setState({ phone: value })}
                                style={[styles.input, styles.summaryInput, !this.payDepoiteEnable() && !this.phoneValid() ? styles.summaryInputWrong : {}]} placeholder="Phone Number (mandatory)" />
                            <Icon name={"mobile-icon"} color={kit.colors.icon.dark} size={RF(4)} style={{ position: "absolute", paddingRight: 15 }} />
                        </View>}

                        <View style={{ marginHorizontal: 10, flexDirection: "row", justifyContent: "flex-end", alignItems: "center" }}>
                            <TextInput
                                maxLength={1000}
                                editable={!this.state.loading}
                                value={this.state.notes}
                                onChangeText={(value) => this.setState({ notes: value })}
                                style={[styles.input, styles.summaryInput]} multiline placeholder="Parking Notes (optional)"
                                onContentSizeChange={(e) => this.updateSize(e.nativeEvent.contentSize.height)}
                            />
                            <Icon name={"comment-icon"} color={kit.colors.icon.dark} size={RF(4)} style={{ position: "absolute", paddingRight: 15 }} />
                        </View>

                    </View>
                    <View style={styles.helpCard} >
                        <View style={styles.summaryItem} >
                            <Text>Number of items</Text>
                            <LinearGradient
                                start={{ x: 0, y: 0 }}
                                end={{ x: 1, y: 0 }}
                                style={styles.summaryItemValue}
                                colors={[kit.colors.optionsBackground[1], kit.colors.optionsBackground[0]]}>
                                <Text>{this.renderNumberOfItems()}</Text>
                            </LinearGradient>
                        </View>
                        <View style={[styles.helpItem, !this.payDepoiteEnable() && !this.state.number_of_item_confirm ? styles.summaryInputWrong : {}]} >
                            <TouchableHighlight
                                underlayColor={'rgba(255,255,255,0.5)'}
                                style={{ height: '100%', width: "30%", borderTopLeftRadius: 15, borderBottomLeftRadius: 15 }}
                                onPress={() => { this.changeConfirm('items') }}>
                                <View style={{ height: '100%', width: "100%", justifyContent: 'center', alignItems: 'center' }} >
                                    {this.state.number_of_item_confirm ?
                                        <LinearGradient
                                            start={{ x: 0, y: 1 }} end={{ x: 1, y: 1 }}
                                            colors={kit.colors.submitButton}
                                            style={styles.floorItemUnchecked}>
                                            <Icon name="tick-icon-mini" size={RF(2)} color={"#fff"} />
                                        </LinearGradient> :
                                        <LinearGradient
                                            start={{ x: 0, y: 1 }} end={{ x: 1, y: 1 }}
                                            colors={kit.colors.optionsBackground}
                                            style={styles.floorItemUnchecked} />
                                    }
                                </View>
                            </TouchableHighlight>
                            <View style={{ height: '100%', width: "70%", justifyContent: 'center', paddingHorizontal: 6 }} >
                                <Text style={{ fontSize: RF(2.0) }} >I confirm that all furniture appliances and other large items are included in my inventory</Text>
                                <Text style={{ fontSize: RF(1.7) }} >There should be space for a few extra small last minute items</Text>
                            </View>
                        </View>
                        {this.isAdminVoucher() && <View style={[styles.helpItem]} >
                            <TouchableHighlight
                                underlayColor={'rgba(255,255,255,0.5)'}
                                style={{ height: '100%', width: "30%", borderTopLeftRadius: 15, borderBottomLeftRadius: 15 }}
                                onPress={() => { this.changeConfirm('optin') }}>
                                <View style={{ height: '100%', width: "100%", justifyContent: 'center', alignItems: 'center' }} >
                                    {this.state.optin_confirm ?
                                        <LinearGradient
                                            start={{ x: 0, y: 1 }} end={{ x: 1, y: 1 }}
                                            colors={kit.colors.submitButton}
                                            style={styles.floorItemUnchecked}>
                                            <Icon name="tick-icon-mini" size={RF(2)} color={"#fff"} />
                                        </LinearGradient> :
                                        <LinearGradient
                                            start={{ x: 0, y: 1 }} end={{ x: 1, y: 1 }}
                                            colors={kit.colors.optionsBackground}
                                            style={styles.floorItemUnchecked} />
                                    }
                                </View>
                            </TouchableHighlight>
                            <View style={{ height: '100%', width: "70%", justifyContent: 'center', paddingHorizontal: 6 }} >
                                <Text style={{ fontSize: RF(2.0) }} >Please click to confirm that we can stay in touch.</Text>
                                <Text style={{ fontSize: RF(1.7) }} >You can unsubscribe at any time</Text>
                            </View>
                        </View>}
                        {this.isAdminVoucher() && <View style={[styles.helpItem, !this.payDepoiteEnable() && !this.state.terms_confirm ? styles.summaryInputWrong : {}]} >
                            <TouchableHighlight
                                underlayColor={'rgba(255,255,255,0.5)'}
                                style={{ height: '100%', width: "30%", borderTopLeftRadius: 15, borderBottomLeftRadius: 15 }}
                                onPress={() => { this.changeConfirm('terms') }}>
                                <View style={{ height: '100%', width: "100%", justifyContent: 'center', alignItems: 'center' }} >
                                    {this.state.terms_confirm ?
                                        <LinearGradient
                                            start={{ x: 0, y: 1 }} end={{ x: 1, y: 1 }}
                                            colors={kit.colors.submitButton}
                                            style={styles.floorItemUnchecked}>
                                            <Icon name="tick-icon-mini" size={RF(2)} color={"#fff"} />
                                        </LinearGradient> :
                                        <LinearGradient
                                            start={{ x: 0, y: 1 }} end={{ x: 1, y: 1 }}
                                            colors={kit.colors.optionsBackground}
                                            style={styles.floorItemUnchecked} />
                                    }
                                </View>
                            </TouchableHighlight>
                            <View style={{ height: '100%', width: "70%", justifyContent: 'center', paddingHorizontal: 6 }} >
                                <Text style={{ fontSize: RF(2.4), color: 'blue' }}
                                    onPress={() => { this.openWebsite("https://www.manandvanworld.com/about/terms") }}>Terms and conditions</Text>
                                <Text style={{ fontSize: RF(1.7) }} >I agree to the Man and Van Supermarket Terms of Service</Text>
                            </View>
                        </View>}
                    </View>

                    <View style={[styles.helpCard, { marginBottom: 100 }]} >
                        <View style={[styles.summaryItem, { borderBottomColor: 'transparent' }]} >
                            <Text>Total price</Text>
                            <LinearGradient
                                start={{ x: 0, y: 0 }}
                                end={{ x: 1, y: 0 }}
                                style={styles.summaryItemValue}
                                colors={[kit.colors.optionsBackground[1], kit.colors.optionsBackground[0]]}>
                                <Text>£{this.state.total_price}</Text>
                            </LinearGradient>
                        </View>
                        <View style={styles.summaryItem} >
                            <Text>Hire time</Text>
                            <LinearGradient
                                start={{ x: 0, y: 0 }}
                                end={{ x: 1, y: 0 }}
                                style={styles.summaryItemValue}
                                colors={[kit.colors.optionsBackground[1], kit.colors.optionsBackground[0]]}>
                                <Text>{this.renderTime(this.state.company.time[0])} </Text>
                            </LinearGradient>
                        </View>
                        <View style={styles.summaryItem} >
                            <Text>Then</Text>
                            <LinearGradient
                                start={{ x: 0, y: 0 }}
                                end={{ x: 1, y: 0 }}
                                style={styles.summaryItemValue}
                                colors={[kit.colors.optionsBackground[1], kit.colors.optionsBackground[0]]}>
                                <Text>£{(this.state.company.over_time / 2).toFixed(1)} per 30 minutes </Text>
                            </LinearGradient>
                        </View>
                        <View style={styles.summaryItem} >
                            <Text>Amount to pay now</Text>
                            <LinearGradient
                                start={{ x: 0, y: 0 }}
                                end={{ x: 1, y: 0 }}
                                style={styles.summaryItemValue}
                                colors={[kit.colors.optionsBackground[1], kit.colors.optionsBackground[0]]}>
                                <Text>£{this.getDepositeAmount()}</Text>
                            </LinearGradient>
                        </View>
                        <View style={styles.summaryItem} >
                            <Text>Pay on the day</Text>
                            <LinearGradient
                                start={{ x: 0, y: 0 }}
                                end={{ x: 1, y: 0 }}
                                style={styles.summaryItemValue}
                                colors={[kit.colors.optionsBackground[1], kit.colors.optionsBackground[0]]}>
                                <Text>£{this.state.total_price - this.getDepositeAmount()} </Text>
                            </LinearGradient>
                        </View>
                    </View>

                </ScrollView>
                <View style={{ width: "100%", justifyContent: "center", alignItems: "center", position: "absolute", bottom: 0, left: 0 }}>
                    <BottomCard
                        action={
                            <Submit
                                icon="next-arrow-icon"
                                onPress={() => {
                                    this.paypalPaymentInit()
                                }}
                                text={this.getDepositeButtonName()}
                                enabled={this.payDepoiteEnable()} />
                        } />
                    {/* <ActivityIndicator visible={this.state.spinner} size="large" color="#0000ff"  /> */}

                </View>
                <ConfirmModal
                    navigation={Navigation}
                    visible={this.state.openConfirm} message={"Done!"} close={() => {
                        this.setState({ openConfirm: false })
                        Navigation.push(this.props.componentId, { component: { name: "home", id: "home" } })
                    }
                    } />
                <ErrorModal visible={this.state.openError} message={"Try again!"} close={() => this.setState({ openError: false })} />
            </View>
        )
    }
}

const mapStateToProps = state => {
    return {
        pickup: state.pickupReducer.pickup,
        destination: state.destinationReducer.destination,
        voucherid: state.voucherReducer.voucherid,
        options: state.optionsReducer,
        items: state.itemsReducer.items,
        irr_items: state.itemsReducer.irr_items,
        schedule: state.scheduleReducer

    }
}

export default connect(mapStateToProps, {})(ShipmentSummary);