import kit from './kit';
import RF from 'react-native-responsive-fontsize';
export default {
    input: {
        backgroundColor: kit.colors.inputBackground,
        marginTop: 5,
        borderRadius: 10,
        width: "100%",
        color: kit.colors.inputText
    },
    floorMenuHolder: {
        width: '100%',
        flex: 1,
        backgroundColor: 'rgba(0,0,0,0.6)',
        justifyContent: 'center',
        alignItems: 'center'
    },
    floorMenuCard: {
        width: '70%',
        height: '70%',
        backgroundColor: '#f5f5f5',
        justifyContent: 'space-between',
        borderRadius: 15,
        paddingHorizontal: 20,
        paddingVertical: 15,
    },
    voucherCard: {
        width: '70%',

        backgroundColor: '#f5f5f5',
        justifyContent: 'space-between',
        borderRadius: 15,
        paddingHorizontal: 20,
        paddingVertical: 15,
    },
    floorMenuItem: {
        height: RF(6.5),
        borderRadius: 15,
        backgroundColor: '#fff',
        marginVertical: 4,
        justifyContent: 'space-between',
        alignItems: 'center',
        flexDirection: 'row',
        paddingHorizontal: 15
    },
    floorItemUnchecked: {
        width: RF(3),
        borderRadius: RF(3) / 2,
        height: RF(3),
        alignItems: "center",
        paddingBottom: RF(0.8) / 4,
        justifyContent: 'center',
        alignItems: 'center'
    },
    bottomCard: {
        width: "90%",
        backgroundColor: "white",
        borderTopLeftRadius: 15,
        borderTopRightRadius: 15,
        shadowColor:"black",
        shadowOffset:{
            width:0,height:3
        },
        shadowOpacity:0.38,
        shadowRadius:16,
        elevation:10,
        padding: 15
    },
    scheduleCard: {
        width: '90%',
        backgroundColor: '#fff',
        borderRadius: 15,
        marginVertical: 5
        // height: RF(40)
    },
    scheduleCardHeader: {
        height: RF(14),
        borderTopLeftRadius: 15,
        borderTopRightRadius: 15,
        borderBottomWidth: 1,
        borderBottomColor: '#f5f5f5'
    },
    scheduleHeader: {
        width: '100%',
        height: '100%',
        paddingHorizontal: 10,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },

    bottomActionCard: {
        width: "90%",
        marginTop: 15,
        backgroundColor: "white",
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10,
        flexDirection: "column",
        justifyContent: "center",
        padding: 20
    },
    circleIcon: {
        width: RF(10),
        borderRadius: RF(10) / 2,
        height: RF(10),
        alignItems: "center",
        paddingTop: RF(10) / 4
    },
    smallCircleIcon: {
        width: RF(5),
        borderRadius: RF(5) / 2,
        height: RF(5),
        maxHeight: RF(5),
        marginHorizontal:4,
        alignItems: "center",
        paddingTop: (RF(5) / 8)+1
    },
    //CATEGORY ITEM
    categoryItemHolder: {
        borderRadius: 15,
        width: RF(16),
        height: RF(16),
        backgroundColor: '#f3fcff',
        position: 'relative',
        justifyContent: 'center',
        alignItems: 'center'
    },
    categoryLabel: {
        borderBottomLeftRadius: 15,
        borderTopRightRadius: 15,
        width: 27,
        height: 27,
        position: 'absolute',
        top: 0,
        right: 0,
        justifyContent: 'center',
        alignItems: 'center'
    },
    categoryLabelText: {
        color: '#fff'
    },
    categoryFooter: {
        width: '80%',
        position: 'absolute',
        bottom: 0,
        height: RF(1),
        justifyContent: 'center',
        alignItems: 'center',
        borderTopLeftRadius: 5,
        borderTopRightRadius: 5
    },
    //furniture item
    furnitureItem: {
        width: '90%',
        height: RF(8),
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingHorizontal: 15,
        position: 'relative',
        backgroundColor: '#fff',
        marginVertical: 5,
        borderRadius: 10,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 3,
        },
        shadowOpacity: 0.38,
        shadowRadius: 16.00,
        elevation: 5,
    },
    furnitureLabel: {
        height: RF(4),
        width: 5,
        position: 'absolute',
        left: 0,
        top: RF(2),
        borderBottomRightRadius: 2,
        borderTopRightRadius: 2
    },
    furnitureCounter: {
        height: RF(5),
        borderRadius: 15,
        alignItems: 'center',
        justifyContent: 'space-between',
        flexDirection: 'row',
        paddingHorizontal: 0
    },
    newFurniture: {
        marginTop: 15,
        marginBottom: 5,
        width: '90%',
        height: RF(24),
        borderRadius: 15,
        backgroundColor: '#fff',
        flexDirection: 'row'
    },
    newInputHolder: {
        width: RF(11),
        height: RF(5),
        borderRadius: 10,
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'row',
        paddingHorizontal: RF(1)
    },
    newInputHolderName: {
        width: RF(30),
        height: RF(5),
        borderRadius: 10,
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'row',
        paddingHorizontal: RF(1)
    },
    gridRow:{
        width: '100%',
        paddingHorizontal: 10,
        borderRightWidth: 0.5,
        borderRightColor: '#f5f5f5',
        height: '100%', 
        flexDirection: 'row', 
        justifyContent: 'space-between', 
        alignItems: 'center'
    },
    newGridBordered: { 
        width: '50%',
        paddingHorizontal: 10,
        borderRightWidth: 0.5,
        borderRightColor: '#f5f5f5',
        height: '100%', 
        flexDirection: 'row', 
        justifyContent: 'space-between', 
        alignItems: 'center'
    },
    newGrid: { 
        width: '50%',
        paddingHorizontal: 10,
        height: '100%', 
        flexDirection: 'row', 
        justifyContent: 'space-between', 
        alignItems: 'center'
    },
    // Shipment summary
    summaryItem: {
        width: '100%',
        minHeight: RF(8),
        borderBottomColor: '#f5f5f5',
        borderBottomWidth: 0.5,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        padding: 10
    },
    summaryItemText:{
        marginHorizontal: RF(1),
        alignItems: 'flex-start',
        alignSelf: 'flex-start'
    },
    
    summaryItemValue: {
        width: '55%',
        minHeight: RF(5),
        borderRadius: 10,
        justifyContent: 'center',
        alignItems: 'center',
        paddingVertical: 5
    },

    summaryItemValueEditable: {
        width: '55%',
        minHeight: RF(5),
        borderRadius: 10,
        justifyContent: 'center',
        alignItems: 'center',
        paddingVertical: 5,
        borderWidth: 0.5,
        borderColor: '#353535',
    },

    summaryInput: {
        backgroundColor: 'rgba(0,0,0,0.017)',
        borderRadius: 10,
        borderWidth: 0.5,
        borderColor: '#f5f5f5',
        paddingLeft: 15,
        paddingRight:52,
        marginLeft: 10,
        marginVertical:0,
        minHeight:RF(6)
    },
    summaryInputWrong:{
        borderRadius: 10,
        borderWidth: 1,
        borderColor: "#f53232",
        borderBottomColor: "#f53232",
        borderBottomWidth: 1,
    },
    summaryInputView:{
        marginHorizontal: 10,
        flexDirection: "row", 
        justifyContent: "flex-end", 
        alignItems: "center"
    },
    // shipment options
    helpCard: {
        width: '90%',
        borderRadius: 15,
        backgroundColor: '#fff',
        marginVertical: 10
    },
    helpItem: {
        width: '100%',
        borderBottomColor: '#f5f5f5',
        borderBottomWidth: 0.5,
        height: RF(14),
        flexDirection: 'row'
    },
    helpItemFooter: {
        width: '100%',
        borderBottomColor: '#f0f0f0',
        borderBottomWidth: 0.5,
        height: RF(7),
        flexDirection: 'row'
    }
}