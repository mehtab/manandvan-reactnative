export let colors = {
    navigation:["rgba(10,207,254,0.85)","rgba(73,90,255,0.85)"],
    icon:{
        dark:"#757575"
    },
    inputBackground:"rgba(250,250,250,1)",
    inputText:"#424242",
    inputText:"#626262",
    brightText:"#00e3ae",
    submitButton:["#9be15d","#00e3ae"],
    disabledSubmitButton:["#969696","#bcbcbc"],
    callButton:["#F7CE67","#FCA681"],

    optionsBackground:["#ebedee","#fdfbfb"],
    currentLocationBackground:"rgba(117,117,117,0.5)",
    currentLocationIcon:"#99999A",
}

export let fonts = {

}

export let sizes = {

}

export default {
    colors,
    fonts,
    sizes
}