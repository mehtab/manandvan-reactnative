import React from 'react';
import { Modal, View, Text, TouchableOpacity } from 'react-native';


export default class Handle extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            open:true
        }
    }
    componentWillReceiveProps(newProps) {

    }
    onToggle(){
        this.setState({open:!this.state.open},()=>{
            this.props.onToggle(this.state.open)
        })
    }
    render() {
        return (
            <TouchableOpacity onPress={()=>this.onToggle()} style={{ width: "100%" ,justifyContent:"center",alignContent:"center",flexDirection:"row" }}>
                <View style={{ width: 30, height: 5, borderRadius: 20, backgroundColor: "#eeeeee", marginBottom: 25 }} />
            </TouchableOpacity>
        )
    }
}