import React from 'react';
import { Modal, View, Text, TouchableOpacity } from 'react-native';
import LinearGradient from "react-native-linear-gradient"
import kit from '../assets/styles/kit.js';
import styles from '../assets/styles/index.js';
import RF from "react-native-responsive-fontsize"
import { createIconSetFromIcoMoon } from 'react-native-vector-icons';
import icoMoonConfig from '../assets/icons/selection.json';

const Icon = createIconSetFromIcoMoon(icoMoonConfig, 'icomoon', 'icomoon.ttf');

export default class CircularItem extends React.Component {
    constructor(props) {
        super(props);
        this.state = {}
    }
    componentWillReceiveProps(newProps) {}
    render() {
        return (
            <TouchableOpacity 
                onPress={(this.props.onPress) ? this.props.onPress : undefined} 
                style={{ flexDirection: "column", justifyContent: "center", alignItems: "center"}}>
                <LinearGradient 
                    start={{ x: 0, y: 1 }} 
                    end={{ x: 1, y: 1 }} 
                    colors={kit.colors.optionsBackground} 
                    style={ this.props.type === 'small' ? styles.smallCircleIcon : styles.circleIcon}>

                    <Icon 
                    name={this.props.icon} 
                    size={this.props.type === 'small' ? RF(3) : RF(4.3)} 
                    color={(this.props.color) ? this.props.color : kit.colors.icon.dark} />
                </LinearGradient>
                {this.props.type === 'small' && 
                    <Text style={{
                        color: kit.colors.brightText, 
                        fontSize: RF(1.5), 
                        marginTop: 5
                    }}>{this.props.text}</Text>
                }
                {this.props.type !== 'small' && 
                    <Text style={{
                        color:  kit.colors.inputText, 
                        fontSize: RF(2), 
                        marginTop: 5 
                    }}>{this.props.text}</Text>
                }


            </TouchableOpacity>
        )
    }
}
