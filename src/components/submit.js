import React from 'react';
import { Modal, View, Text, TouchableOpacity } from 'react-native';
import LinearGradient from "react-native-linear-gradient"
import kit from '../assets/styles/kit.js';
import styles from '../assets/styles/index.js';
import RF from "react-native-responsive-fontsize"

import { createIconSetFromIcoMoon } from 'react-native-vector-icons';
import icoMoonConfig from '../assets/icons/selection.json';

const Icon = createIconSetFromIcoMoon(icoMoonConfig, 'icomoon', 'icomoon.ttf');




export default class Submit extends React.Component {
    constructor(props) {
        super(props);
        this.state = {

        }
    }
    componentWillReceiveProps(newProps) {

    }
    submit(){
        if(this.props.onPress){
            if(this.props.enabled)this.props.onPress()
        }
    }
    render() {
        return (
            <TouchableOpacity style={{ marginBottom: "5%", marginTop: 15, justifyContent: 'center', alignItems: 'center'}} onPress={()=>this.submit()}>
                <LinearGradient start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }} colors={(this.props.enabled)?kit.colors.submitButton:kit.colors.disabledSubmitButton} style={{ width: "100%",
                 padding: 15, 
                borderRadius: 10, justifyContent: (this.props.icon)?"space-between":"center", alignItems: "center", flexDirection: "row" }}>
                    {this.props.icon&&<View />}
                    <Text style={{ color: "white",textAlign:"center" }}>{this.props.text}</Text>
                    {this.props.icon&&<Icon name={this.props.icon} color="white" size={RF(3.4)} />}
                </LinearGradient>
            </TouchableOpacity>
        )
    }
}
