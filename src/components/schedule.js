import React from "react";
import { View, Text, Image, TouchableHighlight } from "react-native";
import styles from "../assets/styles";
import LinearGradient from "react-native-linear-gradient";
import { createIconSetFromIcoMoon } from "react-native-vector-icons";
import kit from "../assets/styles/kit.js";
import icoMoonConfig from "../assets/icons/selection.json";
import RF from "react-native-responsive-fontsize";
const Icon = createIconSetFromIcoMoon(icoMoonConfig, "icomoon", "icomoon.ttf");
import { Calendar, CalendarList, Agenda } from "react-native-calendars";
export default class Schedule extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedDate: "",
      company: "",
      selectedCompany: this.props.selectedCompany,
      selectedDate: this.props.selectedDate,
    };
  }

  componentWillReceiveProps(newProps) {
    if (newProps.selectedDate !== this.state.selectedDate)
      this.setState({ selectedDate: newProps.selectedDate });
    if (newProps.selectedCompany !== this.state.selectedCompany)
      this.setState({ selectedCompany: newProps.selectedCompany });
  }

  // componentDidMount(){
  //     alert("Schedule")
  // }

  hasPrice(timeStamp) {
    var day = new Date(timeStamp).toDateString();
    let index = this.props.date.findIndex((item) => {
      let checkDay = new Date(item.date).toDateString();
      return day === checkDay;
    });
    return index;
  }
  renderVan(type) {
    let van = {};
    switch (type) {
      case 0:
        van = {
          title: "Caddy Van",
          icon: require("../assets/images/vans/van-small-icon.png"),
        };
        break;
      case 1:
        van = {
          title: "Transit Van",
          icon: require("../assets/images/vans/van-medium-icon.png"),
        };
        break;
      case 2:
        van = {
          title: "Sprinter van",
          icon: require("../assets/images/vans/van-large-icon.png"),
        };
        break;
      case 3:
        van = {
          title: "Luton Van",
          icon: require("../assets/images/vans/van-giant-icon.png"),
        };
        break;
      default:
        van = {
          title: "Caddy Van",
          icon: require("../assets/images/vans/van-small-icon.png"),
        };
    }
    return van;
  }

  renderDrivers() {
    console.log("drivers ", this.props.drivers);
    return this.props.drivers.map((item, index) => {
      return (
        <Image
          key={index}
          source={{ uri: env.imageUrl + item.picture }}
          style={{
            width: RF(8),
            height: RF(8),
            borderRadius: RF(4),
            marginRight: 2,
          }}
        />
      );
    });
  }

  render() {
    return (
      <View
        style={[
          styles.scheduleCard,
          {
            borderColor: this.state.selectedDate ? "#9be15d" : "transparent",
            borderWidth: 1,
          },
        ]}
      >
        <View style={styles.scheduleCardHeader}>
          <View style={styles.scheduleHeader}>
            <View style={{ flexDirection: "row", alignItems: "center" }}>
              <Image
                source={{ uri: env.imageUrl + this.props.logo }}
                style={{ borderRadius: 5, width: RF(9), height: RF(9) }}
              />
              <Text style={{ marginLeft: 5 }}>{this.props.name}</Text>
            </View>
            <View style={{ flexDirection: "row", alignItems: "center" }}>
              <Icon
                name="destination-icon"
                size={25}
                style={{ marginRight: 5 }}
                color={kit.colors.icon.dark}
              />
              <Text>{this.props.postCode}</Text>
            </View>
          </View>
        </View>
        <View style={styles.scheduleCardHeader}>
          <View style={styles.scheduleHeader}>
            <View style={{ flexDirection: "row", alignItems: "center" }}>
              <Image
                source={this.renderVan(this.props.van).icon}
                style={{ width: RF(11), height: RF(11) }}
              />
              <Text style={{ marginLeft: 5 }}>
                {this.renderVan(this.props.van).title}
              </Text>
            </View>
            <View style={{ flexDirection: "row", alignItems: "center" }}>
              {this.renderDrivers()}
            </View>
          </View>
        </View>
        <Calendar
          // hideArrows={true}-
          minDate={new Date().toISOString().split("T")[0]}
          maxDate={
            new Date(new Date().getTime() + 30 * 24 * 3600000)
              .toISOString()
              .split("T")[0]
          }
          renderArrow={(direction) => {
            return (
              <Icon
                style={{
                  borderRadius: 15,
                  padding: 6,
                  borderColor: "#605f5a",
                  borderWidth: 2,
                }}
                size={15}
                name={
                  direction === "right" ? "next-arrow-icon" : "back-arrow-icon"
                }
                color="#605f5a"
              />
            );
          }}
          hideExtraDays
          markedDates={{}}
          style={{ borderBottomLeftRadius: 17, borderBottomRightRadius: 17 }}
          dayComponent={(data) => {
            let hasPrice = this.hasPrice(data.date.timestamp);
            let weekDay = new Date(data.date.dateString).getDay();
            if (hasPrice > -1)
              return (
                <TouchableHighlight
                  underlayColor={"rgba(255,255,255,0.4)"}
                  onPress={() => {
                    this.setState(
                      {
                        selectedDate:
                          this.state.selectedDate == data.date.dateString
                            ? ""
                            : data.date.dateString,
                        selectedCompany: this.props.name,
                      },
                      () => {
                        this.props.onChange(
                          this.state.selectedDate,
                          this.state.selectedCompany
                        );
                      }
                    );
                  }}
                >
                  {this.state.selectedDate == data.date.dateString &&
                  this.props.name === this.state.selectedCompany ? (
                    <LinearGradient
                      style={{
                        width: 45,
                        height: 50,
                        borderRadius: 15,
                        justifyContent: "center",
                        alignItems: "center",
                      }}
                      start={{ x: 0, y: 0 }}
                      end={{ x: 1, y: 0 }}
                      colors={kit.colors.submitButton}
                    >
                      <Text style={{ fontSize: 10, color: "#fff" }}>
                        {data.date.day}
                      </Text>
                      <Text style={{ color: "#fff", fontSize: 11 }}>
                        £{this.props.date[hasPrice].price}
                      </Text>
                    </LinearGradient>
                  ) : (
                    <LinearGradient
                      style={{
                        width: 45,
                        height: 50,
                        borderRadius: 15,
                        justifyContent: "center",
                        alignItems: "center",
                      }}
                      start={{ x: 0, y: 0 }}
                      end={{ x: 1, y: 0 }}
                      colors={["transparent", "transparent"]}
                    >
                      <Text style={{ fontSize: 10, color: "#c5c5c5" }}>
                        {data.date.day}
                      </Text>
                      <Text
                        style={{
                          color:
                            weekDay == 0 || weekDay == 6 ? "red" : undefined,
                          fontSize: 11,
                        }}
                      >
                        £{Math.floor(this.props.date[hasPrice].price)}
                      </Text>
                    </LinearGradient>
                  )}
                </TouchableHighlight>
              );
            else return null;
          }}
        />
      </View>
    );
  }
}
