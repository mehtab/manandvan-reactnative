import { Modal, View, Text, TouchableOpacity, ScrollView, TextInput, Image } from 'react-native';
import styles from '../assets/styles';
import React, {useState} from 'react'
import LinearGradient from "react-native-linear-gradient"
import { createIconSetFromIcoMoon } from 'react-native-vector-icons';
import kit from '../assets/styles/kit.js';
import icoMoonConfig from '../assets/icons/selection.json';
import RF from 'react-native-responsive-fontsize';
import Line from "../components/line"
import Submit from '../components/submit.js';

const Icon = createIconSetFromIcoMoon(icoMoonConfig, 'icomoon', 'icomoon.ttf');

export default function GetVendorCode(props) {
    const [visible, setVisible] = useState(false)
  return (
   <Modal transparent visible={visible} onRequestClose={() => { props.close() }}>
       <View style={styles.floorMenuHolder} >
                    <View style={[styles.voucherCard, { backgroundColor: "white", width: "90%" }]} >
                        <TouchableOpacity onPress={() => props.close()}>
                            <Icon name="close-circle-icon" color="#D7D7D7" size={RF(4.7)} />
                        </TouchableOpacity>
                        <Icon name="tick-icon-mini" color="green" size={RF(14)} style={{ marginTop: 40, width: "30%", alignSelf: "center" }} resizeMode={"contain"}/>
                        <Submit 
                            // onPress={() => this.submit()} 
                            text={"Hi"} 
                            enabled={true} />

                        <Text style={{ textAlignVertical: "center", fontSize: 10 }}>You will get confirmation email shortly.</Text>
                        <Line />
                
                        <View style={{ flexDirection: "row", paddingVertical: 20, justifyContent: "space-between", alignItems: "baseline" }}>
                            <Text style={{ textAlignVertical: "center", fontSize: 10 }}>If there is any problem, call us</Text>
                            <TouchableOpacity 
                            // onPress={() => callSupport("admin")} 
                            >
                                <LinearGradient
                                    start={{ x: 0, y: 1 }} end={{ x: 1, y: 0 }}
                                    colors={kit.colors.callButton}
                                    style={{ padding: 7, borderRadius: 10, alignItems: "center" }}>
                                    <Icon name="call-icon" color="white" size={RF(3)} />

                                </LinearGradient>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
   </Modal>
  )
}