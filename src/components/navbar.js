
import React from 'react';
import { Modal, View, Text, TouchableHighlight } from 'react-native';
import LinearGradient from "react-native-linear-gradient"
import kit from '../assets/styles/kit.js';
import { createIconSetFromIcoMoon } from 'react-native-vector-icons';
import icoMoonConfig from '../assets/icons/selection.json';
import RF from "react-native-responsive-fontsize"

const Icon = createIconSetFromIcoMoon(icoMoonConfig, 'icomoon', 'icomoon.ttf');

export default class Navbar extends React.Component {
    constructor(props) {
        super(props);
        this.state = { }
    }
    componentWillReceiveProps(newProps) {}
    render() {
        return (
            <LinearGradient 
            start={{ x: 0, y: 0 }} 
            end={{ x: 1, y: 0 }} 
            colors={kit.colors.navigation}
            style={{ width: "100%", paddingLeft: 10, paddingRight:10, paddingVertical: 20, position: "absolute", top: 0, left: 0,zIndex: 999  }}>
                {!!!this.props.hideTopBar &&
                <View style={{ justifyContent: "space-between", flexDirection: "row" }}>
                    {this.props.leftItem}
                    <Text style={{ color: "white" }}>Man and Van Supermarket</Text>
                    {this.props.rightItem}
                </View>}
                {this.props.children}
            </LinearGradient>
        )
    }
}