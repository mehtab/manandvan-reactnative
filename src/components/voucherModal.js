import React from 'react';
import { Modal, View, Text, TouchableOpacity, ScrollView, TextInput, Image } from 'react-native';
import styles from '../assets/styles';
import LinearGradient from "react-native-linear-gradient"
import { createIconSetFromIcoMoon } from 'react-native-vector-icons';
import kit from '../assets/styles/kit.js';
import icoMoonConfig from '../assets/icons/selection.json';
import RF from 'react-native-responsive-fontsize';
import Line from "../components/line"
import Submit from '../components/submit.js';
import { request,callSupport } from "../tools"


const Icon = createIconSetFromIcoMoon(icoMoonConfig, 'icomoon', 'icomoon.ttf');
export default class VoucherModal extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            visible: this.props.visible ? this.props.visible : false,
            voucherCode: null,
            voucherStatus: null,
            loading: false,
            voucherFormatValid: false
        }
    }

    componentWillReceiveProps(newProps) {
        this.setState({ visible: newProps.visible });
    }

    submit() {
        this.setState({ loading: true })
        request("/app/api/v1/voucher/validate", { "voucher_id": this.state.voucherCode }, (err, result) => {
            if (err) return this.setState({ voucherStatus: "wrong", loading: false })
            if (result.code == 200) {
                this.setState({ voucherStatus: "correct", loading: false })
                this.props.onVoucherApplied(this.state.voucherCode)
            }
            else return this.setState({ voucherStatus: "wrong", loading: false })
        })
    }
    
    _voucherChanged(voucher) {
        this.setState({ voucherCode: voucher, voucherStatus: null,voucherFormatValid: voucher.length === 8 })
    }
    inputBackground(status) {
        switch (status) {
            case "wrong":
                return "#FBE9E7"
            default:
                return "#F5F5F5"
        }
    }
    render() {
        return (
            <Modal transparent visible={this.state.visible} onRequestClose={() => { this.props.close() }}>
                <View style={styles.floorMenuHolder} >
                    <View style={[styles.voucherCard, { backgroundColor: "white", width: "90%" }]} >
                        <TouchableOpacity onPress={() => this.props.close()}>
                            <Icon name="close-circle-icon" color="#D7D7D7" size={RF(4.7)} />
                        </TouchableOpacity>
                        <Image source={require("../assets/images/voucher.png")} style={{ width: "20%", alignSelf: "center" }} resizeMode={"contain"} />
                        <View style={{ flexDirection: "row", justifyContent: "flex-end", alignItems: "center" }}>

                            <TextInput
                                maxLength={8}
                                keyboardType = 'numeric'
                                editable={!this.state.loading} 
                                onChangeText={(voucher) => this._voucherChanged(voucher)} 
                                style={[styles.input, { borderWidth: (this.state.voucherStatus == "wrong") ? 1 : 0, borderColor: (this.state.voucherStatus == "wrong") ? "#FFC0AA" : null, backgroundColor: (this.state.voucherStatus == "wrong") ? "#FBE9E7" : "#F5F5F5", padding: 15 }]} 
                                value={this.state.voucherCode} 
                                placeholder="Enter your membership number" />
                            {this.state.voucherStatus && <Icon name={(this.state.voucherStatus == "wrong") ? "error-icon" : "tick-icon"} color={(this.state.voucherStatus == "wrong") ? "#ff7043" : "#00c853"} size={RF(4)} style={{ position: "absolute", paddingRight: 15 }} />}
                        </View>
                        <Submit 
                            onPress={() => this.submit()} 
                            text={(this.state.loading) ? "Hold on" : "Enter number"} 
                            enabled={this.state.voucherFormatValid && this.state.voucherCode && this.state.voucherStatus != "wrong" && !this.state.loading} />
                        <Line />

                        <View style={{ flexDirection: "row", paddingVertical: 20, justifyContent: "space-between", alignItems: "baseline" }}>
                            <Text style={{ textAlignVertical: "center", fontSize: 10 }}>If you don't have one, call us</Text>
                            <TouchableOpacity onPress={() => callSupport("admin")} >
                                <LinearGradient
                                    start={{ x: 0, y: 1 }} end={{ x: 1, y: 0 }}
                                    colors={kit.colors.callButton}
                                    style={{ padding: 7, borderRadius: 10, alignItems: "center" }}>
                                    <Icon name="call-icon" color="white" size={RF(3)} />

                                </LinearGradient>
                            </TouchableOpacity>

                        </View>
                    </View>
                </View>
            </Modal >
        )
    }
}