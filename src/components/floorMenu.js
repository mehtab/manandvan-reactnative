import React from 'react';
import { Modal, View, Text, TouchableOpacity,ScrollView } from 'react-native';
import styles from '../assets/styles';
import LinearGradient from "react-native-linear-gradient"
import { createIconSetFromIcoMoon } from 'react-native-vector-icons';
import kit from '../assets/styles/kit.js';
import icoMoonConfig from '../assets/icons/selection.json';
import RF from 'react-native-responsive-fontsize';
const Icon = createIconSetFromIcoMoon(icoMoonConfig, 'icomoon', 'icomoon.ttf');

export default class FloorMenu extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedFloor: 0,
            visible: this.props.visible ? this.props.visible : false
        }
    }

    componentWillReceiveProps(newProps) {
        this.setState({visible: newProps.visible});
    }

    onSubmit() {
        if (this.props.onSelect)
            return this.props.onSelect(this.state.selectedFloor)
    }

    render() {
        return (
            <Modal transparent visible={this.state.visible} onRequestClose={() => { }}>
                <View style={styles.floorMenuHolder} >
                    <View style={styles.floorMenuCard} >
                        <ScrollView contentContainerStyle={{  }} >
                            <TouchableOpacity onPress={() => { this.setState({ selectedFloor: -1 }) }} style={{ borderRadius: 10 }} underlayColor={'rgba(255,255,255,0.5)'}>
                                <View style={styles.floorMenuItem} >
                                    <Text>Basement</Text>
                                    {this.state.selectedFloor === -1 ?
                                        <LinearGradient
                                            start={{ x: 0, y: 1 }} end={{ x: 1, y: 1 }}
                                            // colors={kit.colors.optionsBackground}
                                            colors={kit.colors.submitButton}
                                            style={styles.floorItemUnchecked}>
                                            <Icon name="tick-icon-mini" size={RF(2)} color={"#fff"} />
                                        </LinearGradient> :
                                        <LinearGradient
                                            start={{ x: 0, y: 1 }} end={{ x: 1, y: 1 }}
                                            colors={kit.colors.optionsBackground}
                                            style={styles.floorItemUnchecked} />
                                    }
                                </View>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => { this.setState({ selectedFloor: 0 }) }} style={{ borderRadius: 10 }} underlayColor={'rgba(255,255,255,0.5)'}>
                                <View style={styles.floorMenuItem} >
                                    <Text>Ground Floor</Text>
                                    {this.state.selectedFloor === 0 ?
                                        <LinearGradient
                                            start={{ x: 0, y: 1 }} end={{ x: 1, y: 1 }}
                                            // colors={kit.colors.optionsBackground}
                                            colors={kit.colors.submitButton}
                                            style={styles.floorItemUnchecked}>
                                            <Icon name="tick-icon-mini" size={RF(2)} color={"#fff"} />
                                        </LinearGradient> :
                                        <LinearGradient
                                            start={{ x: 0, y: 1 }} end={{ x: 1, y: 1 }}
                                            colors={kit.colors.optionsBackground}
                                            style={styles.floorItemUnchecked} />
                                    }
                                </View>
                            </TouchableOpacity>

                            <TouchableOpacity onPress={() => { this.setState({ selectedFloor: 1 }) }} style={{ borderRadius: 10 }} underlayColor={'rgba(255,255,255,0.5)'}>
                                <View style={styles.floorMenuItem} >
                                    <Text>First floor</Text>
                                    {this.state.selectedFloor === 1 ?
                                        <LinearGradient
                                            start={{ x: 0, y: 1 }} end={{ x: 1, y: 1 }}
                                            // colors={kit.colors.optionsBackground}
                                            colors={kit.colors.submitButton}
                                            style={styles.floorItemUnchecked}>
                                            <Icon name="tick-icon-mini" size={RF(2)} color={"#fff"} />
                                        </LinearGradient> :
                                        <LinearGradient
                                            start={{ x: 0, y: 1 }} end={{ x: 1, y: 1 }}
                                            colors={kit.colors.optionsBackground}
                                            style={styles.floorItemUnchecked} />
                                    }
                                </View>
                            </TouchableOpacity>

                            <TouchableOpacity onPress={() => { this.setState({ selectedFloor: 2 }) }} style={{ borderRadius: 10 }} underlayColor={'rgba(255,255,255,0.5)'}>
                                <View style={styles.floorMenuItem} >
                                    <Text>Second floor</Text>
                                    {this.state.selectedFloor === 2 ?
                                        <LinearGradient
                                            start={{ x: 0, y: 1 }} end={{ x: 1, y: 1 }}
                                            // colors={kit.colors.optionsBackground}
                                            colors={kit.colors.submitButton}
                                            style={styles.floorItemUnchecked}>
                                            <Icon name="tick-icon-mini" size={RF(2)} color={"#fff"} />
                                        </LinearGradient> :
                                        <LinearGradient
                                            start={{ x: 0, y: 1 }} end={{ x: 1, y: 1 }}
                                            colors={kit.colors.optionsBackground}
                                            style={styles.floorItemUnchecked} />
                                    }
                                </View>
                            </TouchableOpacity>

                            <TouchableOpacity onPress={() => { this.setState({ selectedFloor: 3 }) }} style={{ borderRadius: 10 }} underlayColor={'rgba(255,255,255,0.5)'}>
                                <View style={styles.floorMenuItem} >
                                    <Text>Third floor</Text>
                                    {this.state.selectedFloor === 3 ?
                                        <LinearGradient
                                            start={{ x: 0, y: 1 }} end={{ x: 1, y: 1 }}
                                            // colors={kit.colors.optionsBackground}
                                            colors={kit.colors.submitButton}
                                            style={styles.floorItemUnchecked}>
                                            <Icon name="tick-icon-mini" size={RF(2)} color={"#fff"} />
                                        </LinearGradient> :
                                        <LinearGradient
                                            start={{ x: 0, y: 1 }} end={{ x: 1, y: 1 }}
                                            colors={kit.colors.optionsBackground}
                                            style={styles.floorItemUnchecked} />
                                    }
                                </View>
                            </TouchableOpacity>

                            <TouchableOpacity onPress={() => { this.setState({ selectedFloor: 4 }) }} style={{ borderRadius: 10 }} underlayColor={'rgba(255,255,255,0.5)'}>
                                <View style={styles.floorMenuItem} >
                                    <Text>Fourth floor</Text>
                                    {this.state.selectedFloor === 4 ?
                                        <LinearGradient
                                            start={{ x: 0, y: 1 }} end={{ x: 1, y: 1 }}
                                            // colors={kit.colors.optionsBackground}
                                            colors={kit.colors.submitButton}
                                            style={styles.floorItemUnchecked}>
                                            <Icon name="tick-icon-mini" size={RF(2)} color={"#fff"} />
                                        </LinearGradient> :
                                        <LinearGradient
                                            start={{ x: 0, y: 1 }} end={{ x: 1, y: 1 }}
                                            colors={kit.colors.optionsBackground}
                                            style={styles.floorItemUnchecked} />
                                    }
                                </View>
                            </TouchableOpacity>

                            <TouchableOpacity onPress={() => { this.setState({ selectedFloor: 5 }) }} style={{ borderRadius: 10 }} underlayColor={'rgba(255,255,255,0.5)'}>
                                <View style={styles.floorMenuItem} >
                                    <Text>Fifth floor or more</Text>
                                    {this.state.selectedFloor === 5 ?
                                        <LinearGradient
                                            start={{ x: 0, y: 1 }} end={{ x: 1, y: 1 }}
                                            // colors={kit.colors.optionsBackground}
                                            colors={kit.colors.submitButton}
                                            style={styles.floorItemUnchecked}>
                                            <Icon name="tick-icon-mini" size={RF(2)} color={"#fff"} />
                                        </LinearGradient> :
                                        <LinearGradient
                                            start={{ x: 0, y: 1 }} end={{ x: 1, y: 1 }}
                                            colors={kit.colors.optionsBackground}
                                            style={styles.floorItemUnchecked} />
                                    }
                                </View>
                            </TouchableOpacity>
                        </ScrollView>
                        <TouchableOpacity onPress={() => {this.onSubmit()}} style={{ borderRadius: 10,marginTop:24 }} underlayColor={'rgba(255,255,255,0.5)'}>
                            <LinearGradient
                                start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }}
                                colors={kit.colors.submitButton}
                                style={{ width: "100%", padding: 15, borderRadius: 10, justifyContent: "space-between", alignItems: "center" }}>
                                <Text style={{ color: "white" }}>Select</Text>
                            </LinearGradient>
                        </TouchableOpacity>
                    </View>
                </View>
            </Modal >
        )
    }
}