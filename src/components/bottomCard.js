import React from 'react';
import { Modal, View, Text, TouchableOpacity } from 'react-native';
import LinearGradient from "react-native-linear-gradient"
import kit from '../assets/styles/kit.js';
import styles from '../assets/styles/index.js';
import RF from "react-native-responsive-fontsize"
import Line from "./line"
import Handle from "./handle"

export default class BottomCard extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            bottomItemsOpen: true,
        }
    }
    componentWillReceiveProps(newProps) {

    }
    submit() {
        if (this.props.onPress) {
            if (this.props.enabled) this.props.onPress()
        }
    }
    render() {
        return (
            <View style={{
                width: "100%", position: "absolute", bottom: 0, left: 0, justifyContent: "center", alignItems: "center", shadowColor: "#000",
                shadowOffset: {
                    width: 0,
                    height: 3,
                },
                shadowOpacity: 0.38,
                shadowRadius: 16.00,
                elevation: 6,
            }}>
                <View style={styles.bottomCard}>
                    {this.props.body&&<Handle onToggle={(open) => this.setState({ bottomItemsOpen: open })} />}

                    {this.state.bottomItemsOpen &&
                        this.props.body
                    }
                    {this.props.body&&<Line />}
                    {this.props.action}
                </View>
            </View>

        )
    }
}
