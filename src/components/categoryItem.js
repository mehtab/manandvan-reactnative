import React from 'react';
import { View, Text, Image, TouchableHighlight } from 'react-native';
import styles from '../assets/styles';
import icoMoonConfig from '../assets/icons/selection.json';
import { createIconSetFromIcoMoon } from 'react-native-vector-icons';
import RF from "react-native-responsive-fontsize"
import kit from '../assets/styles/kit.js';

const Icon = createIconSetFromIcoMoon(icoMoonConfig, 'icomoon', 'icomoon.ttf');

export default class CategoryItem extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            count: this.props.count
        }
    }

    render() {
        return (
            <TouchableHighlight onPress={() => this.props.onPress()} style={{borderRadius: 15}} underlayColor={'rgba(255,255,255,0.5)'}>
                <View style={styles.categoryItemHolder}>
                    <Icon name={this.props.image} size={RF(6.3)} color={kit.colors.icon.dark} />
                    <Text style={{ fontSize: 11 }} >{this.props.title}</Text>

                    <View style={[styles.categoryLabel, { backgroundColor: this.props.color }]} >
                        <Text style={styles.categoryLabelText} >{this.props.count}</Text>
                    </View>
                     <View style={[styles.categoryFooter, { backgroundColor: this.props.color }]} /> 
                </View>
            </TouchableHighlight>
        )
    }
}