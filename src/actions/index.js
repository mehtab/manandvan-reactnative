// Actions 
export const setDestination = (payload) => {
    return {type: 'SET_DESTINATION', payload}
}

export const setPickup = (payload) => {
    return {type: "SET_PICKUP", payload}
}

export const setVendorCode = (payload) => {
    return {type: "SET_VENDOR_CODE", payload}
}

export const setVoucher = (payload) => {
    return {type: "SET_VOUCHER_ID", payload}
}

export const setItems = (payload) => {
    return {type: "SET_ITEMS", payload}
}

export const setOptions = (payload) => {
    return {type: "SET_OPTIONS", payload}
}

export const setSchedule = (payload) => {
    return {type: "SET_SCHEDULE", payload}
}